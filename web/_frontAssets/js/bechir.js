$(document).ready(function(){
    $('body').on('click','.deleteBtn', function(e){
        var path = $(this).data('url')
        var element = $(this)
        $.ajax({
            url: path,
            dataType: "json",
            contentType: "application/json",
            success: function (result)
            {
                element.parent().parent().remove()
                //console.log(result)
            },
            error: function (request,error)
            {
                alert("Veuillez contacter l'administrateur de l'application");
            }

        });
        return false;
    })

})