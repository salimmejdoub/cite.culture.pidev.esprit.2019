-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: May 16, 2020 at 02:12 PM
-- Server version: 5.7.24
-- PHP Version: 7.3.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `citeculture`
--

-- --------------------------------------------------------

--
-- Table structure for table `answers`
--

DROP TABLE IF EXISTS `answers`;
CREATE TABLE IF NOT EXISTS `answers` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question_id` int(11) DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci,
  `score` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_50D0C6061E27F6BF` (`question_id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `answers`
--

INSERT INTO `answers` (`id`, `question_id`, `content`, `score`) VALUES
(1, 1, 'Q1', 2),
(2, 1, 'Q2', 1),
(3, 2, 'R1', 2),
(4, 2, 'R2', 1),
(5, 3, 'Le Lorem Ipsum est simplement du faux texte employé dans la composition et la mise en page avant impression', 6),
(6, 3, 'Le Lorem Ipsum est le faux texte standard de l\'imprimerie depuis les années 1500, quand un imprimeur anonyme assembla ensemble des morceaux de texte pour réaliser un livre spécimen de polices de texte', 0),
(7, 3, 'Il n\'a pas fait que survivre cinq siècles, mais s\'est aussi adapté à la bureautique informatique,', 0),
(8, 4, 'On sait depuis longtemps que travailler avec du texte lisible et contenant du sens est source de distractions, et empêche de se concentrer sur la mise en page elle-même', 3),
(9, 4, 'L\'avantage du Lorem Ipsum sur un texte générique comme \'Du texte. Du texte. Du texte.\' est qu\'il possède une distribution de lettres plus ou moins normale, et en tout cas comparable avec celle du français standard.', 0),
(10, 4, 'De nombreuses suites logicielles de mise en page ou éditeurs de sites Web ont fait du Lorem Ipsum', 3),
(11, 5, 'lusieurs variations de Lorem Ipsum peuvent être trouvées ici ou là, mais la majeure partie d\'entre elles a été altérée par l\'addition d\'humour', 1),
(12, 5, 'u de mots aléatoires qui ne ressemblent pas une seconde à du texte standard. Si vous voulez utiliser un passage du Lorem Ipsum,', 0),
(13, 5, 'Tous les générateurs de Lorem Ipsum sur Internet tendent à reproduire le même extrait sans fin, ce qui fait de lipsum.com le seul vrai générateur de Lorem Ipsum.', 5),
(14, 6, 'Tous les jours ou presque', 0),
(15, 6, 'Plusieurs fois par semaine', 0),
(16, 6, '1 fois par semaine', 0),
(17, 6, 'Plus occasionnellement', 0),
(18, 7, 'Tous les jours ou presque', 0),
(19, 7, 'Plusieurs fois par semaine', 0),
(20, 7, '1 fois par semaine', 0),
(21, 7, 'Cela ne m’arrive jamais', 0),
(22, 8, 'Oui, certainement', 0),
(23, 8, 'Oui, probablement', 0),
(24, 8, 'Non, certainement pas', 0),
(25, 9, 'Reponse 1', 0),
(26, 9, 'Reponse 2', 1),
(27, 9, 'Reponse 3', 0),
(28, 10, 'Reponse 1', 1),
(29, 10, 'Reponse 2', 0);

-- --------------------------------------------------------

--
-- Table structure for table `categorie_event`
--

DROP TABLE IF EXISTS `categorie_event`;
CREATE TABLE IF NOT EXISTS `categorie_event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `img` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_CED6E777A76ED395` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categorie_event`
--

INSERT INTO `categorie_event` (`id`, `user_id`, `nom`, `img`) VALUES
(1, 1, 'Colloque Agriculture urbaine', 'hello.png'),
(2, 1, 'Territoire et Consciences au 115ème Congrès des Notaires de France', 'img.png');

-- --------------------------------------------------------

--
-- Table structure for table `client_type`
--

DROP TABLE IF EXISTS `client_type`;
CREATE TABLE IF NOT EXISTS `client_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `score` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `club`
--

DROP TABLE IF EXISTS `club`;
CREATE TABLE IF NOT EXISTS `club` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name_club` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `category_club` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `activity_club` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description_club` longtext COLLATE utf8_unicode_ci,
  `price_club` double DEFAULT NULL,
  `openningHours_club` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `closingHours_club` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `sum_vote` double DEFAULT NULL,
  `nb_vote` int(11) DEFAULT NULL,
  `voters` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `IDX_B8EE3872A76ED395` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `club`
--

INSERT INTO `club` (`id`, `name_club`, `category_club`, `activity_club`, `description_club`, `price_club`, `openningHours_club`, `closingHours_club`, `user_id`, `sum_vote`, `nb_vote`, `voters`) VALUES
(5, 'Créa\'kids', 'Divers', 'Divers', 'Au programme de 8h à 15H les enfants font le tour des activités PEINTURE, POTERIE, BRICOLAGE, CIRQUE et JEUX LUDIQUE.', 100, '08', '15', 1, 6.5, 2, '[1]'),
(6, 'Digital Teens', 'Digital', 'Roboting', 'Notre Summer Camp ‘Digital Teens’ commence le 1er Juillet pour épicer vos vacances avec beaucoup de productivité et surtout beaucoup de nouvelles compétences à maîtriser et pourquoi pas à en faire vos propres petits business!!', 300, '9', '16', 1, 8.5, 2, '[1]'),
(7, 'Aviciennee', 'Divers', 'Divers', 'Plusieurs activités seront proposées à nos enfants à partir du 1er juillet 2019', 420, '10', '19', 1, 4.5, 2, '[1]'),
(8, 'P\'tit Artiste', 'Sportif', 'Tennis', 'Les vacances d\'été approchent, profitez de notre programme riche et épanouissant', 400, '12', '20', 1, 8, 2, '[1]'),
(9, 'Jeune Créateurs', 'Artistique', 'Peinture', 'nous vous proposons notre programme vacances et ce du Lundi 1 juillet au Vendredi 26 juillet de 7h30 à 14h30.PS : Les inscriptions seront ouvertes du samedi 18 mai au samedi 29 juin.Pour réserver, veuillez payer une avance de 50% durant cette période.', 500, '10', '19', 1, 7.5, 2, '[1]'),
(10, 'Les acrobates', 'Divers', 'Divers', 'ACCÈS A TOUTES LES ACTIVITÉS: chant, Danse, Théâtre, Peinture,dessin Bricolage, poterie ,Club de langue, board games, cooking club, projection de films sur écran géant .', 100, '9', '18', 1, 6.5, 2, '[1]');

-- --------------------------------------------------------

--
-- Table structure for table `club_voucher`
--

DROP TABLE IF EXISTS `club_voucher`;
CREATE TABLE IF NOT EXISTS `club_voucher` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `club_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `insertat` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_1EDA005A61190A32` (`club_id`),
  KEY `IDX_1EDA005AA76ED395` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `club_voucher`
--

INSERT INTO `club_voucher` (`id`, `club_id`, `user_id`, `code`, `insertat`) VALUES
(2, 5, 1, '5e69066a8a4ce2ce412b50d490718d82714290dc', '2019-07-06 01:38:18'),
(3, 7, 1, 'f37bb34fad9cf9d689176b4ac5d88f6c7c62b7f7', '2019-07-06 01:39:14');

-- --------------------------------------------------------

--
-- Table structure for table `comment`
--

DROP TABLE IF EXISTS `comment`;
CREATE TABLE IF NOT EXISTS `comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `thread_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `body` longtext COLLATE utf8_unicode_ci NOT NULL,
  `ancestors` varchar(1024) COLLATE utf8_unicode_ci NOT NULL,
  `depth` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `state` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_9474526CE2904019` (`thread_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `competition`
--

DROP TABLE IF EXISTS `competition`;
CREATE TABLE IF NOT EXISTS `competition` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `club_id` int(11) NOT NULL,
  `name_competition` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `visibility_competition` tinyint(1) NOT NULL,
  `startDate_competition` datetime DEFAULT NULL,
  `endDate_competition` datetime DEFAULT NULL,
  `description_competition` longtext COLLATE utf8_unicode_ci,
  `place_competition` longtext COLLATE utf8_unicode_ci,
  `contact_competition` longtext COLLATE utf8_unicode_ci,
  `media_id` int(11) NOT NULL,
  `insertat` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_B50A2CB161190A32` (`club_id`),
  KEY `IDX_B50A2CB1EA9FDD75` (`media_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `competition`
--

INSERT INTO `competition` (`id`, `club_id`, `name_competition`, `visibility_competition`, `startDate_competition`, `endDate_competition`, `description_competition`, `place_competition`, `contact_competition`, `media_id`, `insertat`) VALUES
(3, 5, 'Compétition A', 0, '2019-07-07 04:00:00', '2019-07-07 05:00:00', 'C\'est une compétition nationale dédiée  à tous les amateurs et les passionnés. il suffit d\'un simple clique de participation nous vous répondons par un mail soit d\'acceptation ou de refus.\r\n\r\n', 'Tunis', '71111222', 19, '2019-07-06 01:44:23'),
(4, 7, 'Compétition B', 0, '2019-06-09 09:00:00', '2019-06-09 10:00:00', 'C\'est une compétition nationale dédiée  à tous les amateurs et les passionnés. il suffit d\'un simple clique de participation nous vous répondons par un mail soit d\'acceptation ou de refus.\r\n\r\n', 'Tunis', '71111222', 20, '2019-07-06 01:45:51'),
(5, 9, 'Compétition C', 0, '2019-07-06 15:00:00', '2019-07-06 16:00:00', 'C\'est une compétition nationale dédiée  à tous les amateurs et les passionnés. il suffit d\'un simple clique de participation nous vous répondons par un mail soit d\'acceptation ou de refus.\r\n\r\n', 'Tunis', '71111222', 21, '2019-07-06 01:47:27'),
(6, 10, 'Compétition D', 0, '2019-07-07 08:00:00', '2019-07-07 12:00:00', 'C\'est une compétition nationale dédiée  à tous les amateurs et les passionnés. il suffit d\'un simple clique de participation nous vous répondons par un mail soit d\'acceptation ou de refus.\r\n\r\n', 'Tunis', '71111222', 22, '2019-07-06 01:48:53'),
(7, 6, 'Compétition E', 0, '2019-07-06 18:00:00', '2019-07-06 19:00:00', 'C\'est une compétition nationale dédiée  à tous les amateurs et les passionnés. il suffit d\'un simple clique de participation nous vous répondons par un mail soit d\'acceptation ou de refus.\r\n\r\n', 'Tunis', '711111222', 23, '2019-07-06 01:50:22'),
(8, 9, 'Compétition F', 0, '2019-07-08 08:00:00', '2019-07-08 09:00:00', 'C\'est une compétition nationale dédiée  à tous les amateurs et les passionnés. il suffit d\'un simple clique de participation nous vous répondons par un mail soit d\'acceptation ou de refus.\r\n\r\n', 'Tunis', '71111222', 24, '2019-07-06 01:51:31');

-- --------------------------------------------------------

--
-- Table structure for table `competition_user`
--

DROP TABLE IF EXISTS `competition_user`;
CREATE TABLE IF NOT EXISTS `competition_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `competition_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `insertat` datetime DEFAULT NULL,
  `status` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_83D0485B7B39D312` (`competition_id`),
  KEY `IDX_83D0485BA76ED395` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `competition_user`
--

INSERT INTO `competition_user` (`id`, `competition_id`, `user_id`, `code`, `insertat`, `status`) VALUES
(4, 7, 1, '342e4bb9f987a46963155acb6f46152fc97d5279', '2019-07-06 01:54:48', 1);

-- --------------------------------------------------------

--
-- Table structure for table `event`
--

DROP TABLE IF EXISTS `event`;
CREATE TABLE IF NOT EXISTS `event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prix` double NOT NULL,
  `heure` time NOT NULL,
  `date` date NOT NULL,
  `lieu` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `categorie` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `limit_max` int(11) NOT NULL,
  `nbr_part` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_3BAE0AA7497DD634` (`categorie`),
  KEY `IDX_3BAE0AA7A76ED395` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `event`
--

INSERT INTO `event` (`id`, `titre`, `description`, `prix`, `heure`, `date`, `lieu`, `categorie`, `user_id`, `limit_max`, `nbr_part`) VALUES
(1, 'Colloque Agriculture urbaine', 'Colloque Agriculture urbaine Description', 60, '00:00:00', '2019-07-06', 'Cite de la culture', 1, 1, 20, 0),
(2, 'Territoire et Consciences présent au Salon des Maires et des Collectivités Locales 2019', 'Territoire et Consciences présent au Salon des Maires et des Collectivités Locales 2019 description', 40, '00:00:00', '2019-07-06', 'Cite de la culture', 1, 1, 20, 0),
(3, 'Colloque Agriculture urbaine (Hôtel de l\'Industrie, Paris 6) - 3 juillet à partir de 14h30', 'Colloque Agriculture urbaine Description', 20, '00:00:00', '2019-07-06', 'Cite de la culture', 2, 1, 90, 0);

-- --------------------------------------------------------

--
-- Table structure for table `fos_user`
--

DROP TABLE IF EXISTS `fos_user`;
CREATE TABLE IF NOT EXISTS `fos_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `confirmation_token` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `Nom` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `Prenom` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_957A647992FC23A8` (`username_canonical`),
  UNIQUE KEY `UNIQ_957A6479A0D96FBF` (`email_canonical`),
  UNIQUE KEY `UNIQ_957A6479C05FB297` (`confirmation_token`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `fos_user`
--

INSERT INTO `fos_user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `confirmation_token`, `password_requested_at`, `roles`, `Nom`, `Prenom`) VALUES
(1, 'salim', 'salim', 'salim@lightmyweb.fr', 'salim@lightmyweb.fr', 1, NULL, '$2y$13$.Wnlu6NNrV.kApZEzkLWkevzfkLaFy08gXuFNXKylDiwEMQXh3bti', '2020-05-16 14:08:56', NULL, NULL, 'a:1:{i:0;s:10:\"ROLE_ADMIN\";}', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

DROP TABLE IF EXISTS `media`;
CREATE TABLE IF NOT EXISTS `media` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `club_id` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `insertat` datetime DEFAULT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_6A2CA10C61190A32` (`club_id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `media`
--

INSERT INTO `media` (`id`, `club_id`, `name`, `insertat`, `path`) VALUES
(5, NULL, 'Capture.PNG', '2019-07-05 21:56:05', '65bf8b0eb4c45d1b836c54fb9bc0bf39.png'),
(6, NULL, 'Capture.PNG', '2019-07-05 22:15:38', '944a8ddf5f4cf75bb4ab3f1c68bd087b.png'),
(8, 5, 'B.png', '2019-07-06 01:21:45', '598791d462edcf00da7cfb6bd9aa00dd.png'),
(9, 5, 'Leaders.jpg', '2019-07-06 01:21:45', '1dfcdd4c6f4eaa5bb79691f40dfe5f94.jpg'),
(10, 6, 'Digital.png', '2019-07-06 01:26:41', 'f50cc3579c695207777ae9cbc8993e55.png'),
(11, 7, 'A.png', '2019-07-06 01:29:15', '00d24015de73cb9ba73c1db8572fbf98.png'),
(12, 7, 'avicenne.jpg', '2019-07-06 01:29:15', 'c03d69a3e6636ae810cc11f56e6e113d.jpg'),
(13, 8, 'Art.png', '2019-07-06 01:31:23', '22a3f7e8a9c96d35b69d76d1300b2d31.png'),
(14, 8, 'csm_affiche_pass_sport_jeunes_2017_c0e7d80618.jpg', '2019-07-06 01:31:23', '9a6f6ee990c7094225ac3f22a608ff6c.jpg'),
(15, 9, 'mousaillon_ete2.png', '2019-07-06 01:34:03', 'a2267fbce2024c78769f00c41f532170.png'),
(16, 9, 'B.png', '2019-07-06 01:34:03', '33d62c73b7bada8911b03c9675e75654.png'),
(17, 10, 'tandem_ete1_2019.jpg', '2019-07-06 01:37:13', '979a5c53bfd7300da0073abaeec7591c.jpg'),
(18, 10, 'Acrobate.png', '2019-07-06 01:37:13', 'fd6f6ac994b3b713591df2222d3a0145.png'),
(19, NULL, 'téléchargement.jpg', '2019-07-06 01:44:23', 'cf65a3457bf0d02e7ddfa47782738ddf.jpeg'),
(20, NULL, 'tsd_summer_contest_header.jpg', '2019-07-06 01:45:51', 'edaf5eafa91f183593bb78ac39e192f5.jpeg'),
(21, NULL, 'images (1).jpg', '2019-07-06 01:47:27', 'a568e1f48b4b2d7ab4380a292a7d4c7a.jpeg'),
(22, NULL, 'D.png', '2019-07-06 01:48:53', '27596031404c6f9390fd9beb379aecdc.png'),
(23, NULL, 'C.png', '2019-07-06 01:50:22', 'c80ef26fd344adf4cdbe3f7c53dbc916.png'),
(24, NULL, 'téléchargement (1).jpg', '2019-07-06 01:51:31', '1dd38fe0ff8dadca0a39d8ab40dbcc14.jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `mediap`
--

DROP TABLE IF EXISTS `mediap`;
CREATE TABLE IF NOT EXISTS `mediap` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `insertat` datetime DEFAULT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mediap`
--

INSERT INTO `mediap` (`id`, `name`, `insertat`, `path`) VALUES
(1, 'download', '2019-06-15 20:04:56', 'a77bda443b3658b903f239f68438c1a9.jpeg'),
(2, 'Capture', '2019-07-05 22:24:07', '2bd5dfaf5d6012487b6df77c5137d074.png'),
(3, '46ac44f093fc4de09964c1dde21db898', '2019-07-05 23:40:14', '228dcd9f83110e009e195e589a6c06f7.jpeg'),
(4, 'b6be5aff60e5826298f434c1e0b78453', '2019-07-05 23:42:44', '7231692f487c16a7de7c1201ddeb674b.jpeg'),
(5, '6db0b0a989ac9e0dc3edb2bb2f3c4b82', '2019-07-05 23:43:37', 'c7c0707b2160ad4c9dd111f88b4595b9.jpeg'),
(8, '359d2a1b4a05576f5665c3faa9d07052', '2019-07-05 23:49:03', '4a80f45504efeaa5eec52fb81691ea13.jpeg'),
(9, '887c9cb972061b1af20befde6d9acfc2', '2019-07-05 23:49:41', '35a02c65862145aca95626d0e13a0d32.jpeg'),
(10, '161aeaa58b12493980b1d946c2aada3c', '2019-07-05 23:50:47', 'c8b6975145538fb0e16dbd5707acf94e.jpeg'),
(11, '5a26c311aadf6df0e8bb57b1961f3aae', '2019-07-05 23:51:26', 'b5e58057db9f633d7776ffd8a78e2b61.jpeg'),
(12, '3e799903497fd76e67fc1b820eee2537', '2019-07-05 23:52:21', '0aa42ef432553e31e715f44d337ae35a.jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

DROP TABLE IF EXISTS `notification`;
CREATE TABLE IF NOT EXISTS `notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci NOT NULL,
  `icon` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `route` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `route_parameters` longtext COLLATE utf8_unicode_ci COMMENT '(DC2Type:array)',
  `notification_date` datetime NOT NULL,
  `seen` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `notification`
--

INSERT INTO `notification` (`id`, `title`, `description`, `icon`, `route`, `route_parameters`, `notification_date`, `seen`) VALUES
(1, 'Nouveau évènement', 'Colloque Agriculture urbaine le Cite de la culture', NULL, 'user_homepage', 'a:1:{s:2:\"id\";i:1;}', '2019-07-06 02:23:54', 0),
(2, 'Nouveau évènement', 'Territoire et Consciences présent au Salon des Maires et des Collectivités Locales 2019 le Cite de la culture', NULL, 'user_homepage', 'a:1:{s:2:\"id\";i:2;}', '2019-07-06 02:26:05', 0),
(3, 'Nouveau évènement', 'Colloque Agriculture urbaine (Hôtel de l\'Industrie, Paris 6) - 3 juillet à partir de 14h30 le Cite de la culture', NULL, 'user_homepage', 'a:1:{s:2:\"id\";i:3;}', '2019-07-06 02:27:40', 0);

-- --------------------------------------------------------

--
-- Table structure for table `participation_event`
--

DROP TABLE IF EXISTS `participation_event`;
CREATE TABLE IF NOT EXISTS `participation_event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL,
  `event_id` int(11) DEFAULT NULL,
  `heure` time NOT NULL,
  `date` date NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_3472872CA76ED395` (`user_id`),
  KEY `IDX_3472872C71F7E88B` (`event_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `projection`
--

DROP TABLE IF EXISTS `projection`;
CREATE TABLE IF NOT EXISTS `projection` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `salle_id` int(11) DEFAULT NULL,
  `image_id` int(11) DEFAULT NULL,
  `Description_Projection` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Categorie_Projection` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Title_Projection` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Duree_Projection` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_Projection` date NOT NULL,
  `Prix_Projection` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_8004C826DC304035` (`salle_id`),
  KEY `IDX_8004C8263DA5256D` (`image_id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `projection`
--

INSERT INTO `projection` (`id`, `salle_id`, `image_id`, `Description_Projection`, `Categorie_Projection`, `Title_Projection`, `Duree_Projection`, `date_Projection`, `Prix_Projection`) VALUES
(3, 3, 3, 'Documentaire, Musical', 'FILM', 'BURN THE STAGE', '120', '2019-08-01', 10),
(4, 4, 4, 'Documentaire', 'FILM', 'COMME UN SEUL HOMME', '100', '2019-08-01', 10),
(5, 6, 5, 'Documentaire', 'FILM', 'SUGAR MAN', '80', '2019-07-01', 15),
(8, 14, 8, 'Peu de temps après la disparition mystérieuse du roi du Danemark, Hamlet vit très mal la montée sur le trône de Claudius, le frère du défunt roi.', 'THEATRE', 'Hamlet', '120', '2019-08-03', 50),
(9, 15, 9, 'Dans une atmosphère de guerre civile, seul l\'amour entre deux jeunes que tout oppose peut conduire la société à reconsidérer rancoeurs et préjugés.', 'THEATRE', 'Roméo et Juliette', '120', '2019-08-06', 60),
(10, 11, 10, 'Animation', 'FILM', 'Coco', '60', '2019-08-04', 15),
(11, 6, 11, 'Documentaire', 'FILM', 'BOHEMIAN RHAPSODY', '120', '2019-08-06', 10),
(12, 9, 12, 'Pièce de théâtre, deux vagabonds, Vladimir et Estragon attendent le mystérieux « Godot », un homme qui leur a promis de les aider.', 'THEATRE', 'En attendant Godot', '150', '2019-08-06', 50);

-- --------------------------------------------------------

--
-- Table structure for table `projet`
--

DROP TABLE IF EXISTS `projet`;
CREATE TABLE IF NOT EXISTS `projet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `titreProjet` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descriptionProjet` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dateSoumission` date NOT NULL,
  `echeanceProjet` date NOT NULL,
  `exigenceProjet` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cibleProjet` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_50159CA912469DE2` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `projet`
--

INSERT INTO `projet` (`id`, `category_id`, `titreProjet`, `descriptionProjet`, `dateSoumission`, `echeanceProjet`, `exigenceProjet`, `cibleProjet`) VALUES
(5, 1, 'Une servitude légale pour l’isolation par l’extérieur', 'Le morcellement de la propriété forestière est un frein à la gestion des bois et forêts qui aggrave le déficit de la filière forêt-bois et rend la gestion forestière durable,', '2019-01-01', '2014-01-01', 'Exigence', 'enfants'),
(6, 2, 'Prévenir les recours contre les autorisations d’urbanisme', 'ement de la propriété forestière est un objectif majeur pour la nation.  Voilà pourquoi il est propos', '2019-05-01', '2014-01-01', 'Exiegence', 'Adultes'),
(7, 2, 'Simplification du regroupement forestier', 'Simplification du regroupement forestier', '2018-01-01', '2014-01-01', 'Exiegnce', 'Entreprise'),
(8, 4, 'Refondre l’usufruit forestier des bois et forêts', 'Refondre l’usufruit forestier des bois et forêts', '2018-01-01', '2014-01-01', 'Exigence', 'Entreprise'),
(9, 2, 'Un fonds de garantie pour le démantèlement des éoliennes', 'Un fonds de garantie pour le démantèlement des éoliennes', '2018-01-01', '2014-01-01', 'Exigence', 'Entreprise');

-- --------------------------------------------------------

--
-- Table structure for table `projet_category`
--

DROP TABLE IF EXISTS `projet_category`;
CREATE TABLE IF NOT EXISTS `projet_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `projet_category`
--

INSERT INTO `projet_category` (`id`, `name`) VALUES
(1, 'name of category'),
(2, 'Une agriculture diversifiée et responsable'),
(3, 'La transition énergétique territoriale'),
(4, 'Une ville moderne et équilibrée');

-- --------------------------------------------------------

--
-- Table structure for table `question`
--

DROP TABLE IF EXISTS `question`;
CREATE TABLE IF NOT EXISTS `question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `survey_id` int(11) DEFAULT NULL,
  `content` longtext COLLATE utf8_unicode_ci,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `position` int(11) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_B6F7494EB3FE509D` (`survey_id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `question`
--

INSERT INTO `question` (`id`, `survey_id`, `content`, `type`, `position`, `createdAt`) VALUES
(1, 1, 'Q1', NULL, 0, NULL),
(2, 1, 'Q2', NULL, 1, NULL),
(3, 2, 'Qu\'est-ce que le Lorem Ipsum?', NULL, 1, NULL),
(4, 2, 'Pourquoi l\'utiliser?', NULL, 2, NULL),
(5, 2, 'Où puis-je m\'en procurer?', NULL, 0, NULL),
(6, 3, 'En moyenne, à quelle fréquence vous arrive-t-il de lire des textes ou articles longs* directement en ligne (sur Internet) ou sur des fichiers stockés dans leurs répertoires ?', NULL, 0, NULL),
(7, 3, 'En moyenne, à quelle fréquence vous arrive-t-il de faire des recherches documentaires via des moteurs de recherche Internet (type Google), pour trouver des informations au sens large sur un sujet (pas une donnée ponctuelle comme une date ou une adresse) ?', NULL, 1, NULL),
(8, 3, '.A la lecture de ces quelques lignes, seriez-vous intéressé(e) par une telle application ?', NULL, 2, NULL),
(9, 4, 'Question 1 Pidev', NULL, 0, NULL),
(10, 4, 'Auestion 2', NULL, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `salle`
--

DROP TABLE IF EXISTS `salle`;
CREATE TABLE IF NOT EXISTS `salle` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `Nom_salle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Etat_salle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `Bloc_salle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `NbreChaise_salle` int(11) NOT NULL,
  `Categorie_salle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `salle`
--

INSERT INTO `salle` (`id`, `Nom_salle`, `Etat_salle`, `Bloc_salle`, `NbreChaise_salle`, `Categorie_salle`) VALUES
(3, 'Opéra_Night', 'NON DISPONIBLE', 'BLOC A', 0, 'Salle_Cinema'),
(4, 'Majestic', 'NON DISPONIBLE', 'BLOC A', 300, 'Salle_Cinema'),
(5, 'Rio-Opera', 'NON DISPONIBLE', 'BLOC A', 300, 'Salle_Théatre'),
(6, 'Capri', 'NON DISPONIBLE', 'BLOC A', 300, 'Salle_Théatre'),
(8, 'le Grand Rex', 'DISPONIBLE', 'BLOC B', 300, 'Salle_Théatre'),
(9, 'Palais Agostini', 'NON DISPONIBLE', 'BLOC B', 300, 'Salle_Théatre'),
(11, 'Electric Cinema', 'NON DISPONIBLE', 'BLOC B', 300, 'Salle_Cinema'),
(12, 'Piscine Edouard Pailleron', 'DISPONIBLE', 'BLOC B', 300, 'Salle_Théatre'),
(14, 'Paramount Theater', 'NON DISPONIBLE', 'BLOC B', 500, 'Salle_Théatre'),
(15, 'Stunning Winter Garden Theatre', 'NON DISPONIBLE', 'BLOC B', 500, 'Salle_Théatre');

-- --------------------------------------------------------

--
-- Table structure for table `survey`
--

DROP TABLE IF EXISTS `survey`;
CREATE TABLE IF NOT EXISTS `survey` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `IDX_AD5F9BFC12469DE2` (`category_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `survey`
--

INSERT INTO `survey` (`id`, `category_id`, `title`, `description`) VALUES
(1, 1, 'sondage 1', 'sondage 1 description'),
(2, 2, 'Lorem Ipsum 1', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam ac sagittis dolor. Nunc lobortis iaculis tellus ut vulputate. Donec nec turpis feugiat, tincidunt quam ac, aliquet nulla. Praesent metus felis, commodo vel magna non, varius gravida nisl. Maecenas id nulla neque. Vivamus eleifend purus quis nulla sollicitudin cursus. Nam varius est et mattis mollis. Sed id viverra sapien, vel ultrices libero. Donec euismod mi sed ligula ullamcorper lobortis.'),
(3, 2, 'Nouvelle application', 'En moyenne, à quelle fréquence vous arrive-t-il de lire des textes ou articles longs* directement en ligne (sur Internet) ou sur des fichiers stockés dans leurs répertoires ?'),
(4, 5, 'Titre sondage pidev', 'Description sondage pidev');

-- --------------------------------------------------------

--
-- Table structure for table `survey_category`
--

DROP TABLE IF EXISTS `survey_category`;
CREATE TABLE IF NOT EXISTS `survey_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `survey_category`
--

INSERT INTO `survey_category` (`id`, `name`) VALUES
(1, 'Categorie 1'),
(2, 'Lorem ipsum'),
(3, 'Aenean sodales'),
(4, 'Praesent molestie'),
(5, 'Categorie test pidev');

-- --------------------------------------------------------

--
-- Table structure for table `survey_result`
--

DROP TABLE IF EXISTS `survey_result`;
CREATE TABLE IF NOT EXISTS `survey_result` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `json` longtext COLLATE utf8_unicode_ci,
  `survey` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `survey_result`
--

INSERT INTO `survey_result` (`id`, `email`, `name`, `json`, `survey`) VALUES
(3, 'salim_mejdoub@hotmail.com', 'Salim Mejdoub', '', 1),
(6, 'salim_mejdoub@hotmail.com', 'Salim Mejdoub', '[{\"answer_id\":13,\"question_id\":5,\"survey_id\":2},{\"answer_id\":8,\"question_id\":4,\"survey_id\":2},{\"answer_id\":5,\"question_id\":3,\"survey_id\":2}]', 2),
(7, 'sa@de.com', 'sa sa', '[{\"answer_id\":2,\"question_id\":1,\"survey_id\":1},{\"answer_id\":3,\"question_id\":2,\"survey_id\":1}]', 1),
(8, 'dorra.abidi@esprit.tn', 'Dorra Abidi', '[{\"answer_id\":11,\"question_id\":5,\"survey_id\":2},{\"answer_id\":8,\"question_id\":4,\"survey_id\":2},{\"answer_id\":5,\"question_id\":3,\"survey_id\":2}]', 2),
(9, 'salim_mejdoub@hotmail.com', 'Salim Mejdoub', '[{\"answer_id\":26,\"question_id\":9,\"survey_id\":4},{\"answer_id\":28,\"question_id\":10,\"survey_id\":4}]', 4);

-- --------------------------------------------------------

--
-- Table structure for table `thread`
--

DROP TABLE IF EXISTS `thread`;
CREATE TABLE IF NOT EXISTS `thread` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permalink` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_commentable` tinyint(1) NOT NULL,
  `num_comments` int(11) NOT NULL,
  `last_comment_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `answers`
--
ALTER TABLE `answers`
  ADD CONSTRAINT `FK_50D0C6061E27F6BF` FOREIGN KEY (`question_id`) REFERENCES `question` (`id`);

--
-- Constraints for table `categorie_event`
--
ALTER TABLE `categorie_event`
  ADD CONSTRAINT `FK_CED6E777A76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user` (`id`);

--
-- Constraints for table `club`
--
ALTER TABLE `club`
  ADD CONSTRAINT `FK_B8EE3872A76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user` (`id`);

--
-- Constraints for table `club_voucher`
--
ALTER TABLE `club_voucher`
  ADD CONSTRAINT `FK_1EDA005A61190A32` FOREIGN KEY (`club_id`) REFERENCES `club` (`id`),
  ADD CONSTRAINT `FK_1EDA005AA76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user` (`id`);

--
-- Constraints for table `comment`
--
ALTER TABLE `comment`
  ADD CONSTRAINT `FK_9474526CE2904019` FOREIGN KEY (`thread_id`) REFERENCES `thread` (`id`);

--
-- Constraints for table `competition`
--
ALTER TABLE `competition`
  ADD CONSTRAINT `FK_B50A2CB161190A32` FOREIGN KEY (`club_id`) REFERENCES `club` (`id`),
  ADD CONSTRAINT `FK_B50A2CB1EA9FDD75` FOREIGN KEY (`media_id`) REFERENCES `media` (`id`);

--
-- Constraints for table `competition_user`
--
ALTER TABLE `competition_user`
  ADD CONSTRAINT `FK_83D0485B7B39D312` FOREIGN KEY (`competition_id`) REFERENCES `competition` (`id`),
  ADD CONSTRAINT `FK_83D0485BA76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user` (`id`);

--
-- Constraints for table `event`
--
ALTER TABLE `event`
  ADD CONSTRAINT `FK_3BAE0AA7497DD634` FOREIGN KEY (`categorie`) REFERENCES `categorie_event` (`id`),
  ADD CONSTRAINT `FK_3BAE0AA7A76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user` (`id`);

--
-- Constraints for table `media`
--
ALTER TABLE `media`
  ADD CONSTRAINT `FK_6A2CA10C61190A32` FOREIGN KEY (`club_id`) REFERENCES `club` (`id`);

--
-- Constraints for table `participation_event`
--
ALTER TABLE `participation_event`
  ADD CONSTRAINT `FK_3472872C71F7E88B` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`),
  ADD CONSTRAINT `FK_3472872CA76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user` (`id`);

--
-- Constraints for table `projection`
--
ALTER TABLE `projection`
  ADD CONSTRAINT `FK_8004C8263DA5256D` FOREIGN KEY (`image_id`) REFERENCES `mediap` (`id`),
  ADD CONSTRAINT `FK_8004C826DC304035` FOREIGN KEY (`salle_id`) REFERENCES `salle` (`id`);

--
-- Constraints for table `projet`
--
ALTER TABLE `projet`
  ADD CONSTRAINT `FK_50159CA912469DE2` FOREIGN KEY (`category_id`) REFERENCES `projet_category` (`id`);

--
-- Constraints for table `question`
--
ALTER TABLE `question`
  ADD CONSTRAINT `FK_B6F7494EB3FE509D` FOREIGN KEY (`survey_id`) REFERENCES `survey` (`id`);

--
-- Constraints for table `survey`
--
ALTER TABLE `survey`
  ADD CONSTRAINT `FK_AD5F9BFC12469DE2` FOREIGN KEY (`category_id`) REFERENCES `survey_category` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
