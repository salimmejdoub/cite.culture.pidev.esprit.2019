<?php

namespace MobileApiBundle\Controller;

use GestEventBundle\Entity\event;
use GestEventBundle\Entity\ParticipationEvent;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class DefaultController extends Controller
{
    public function indexAction()
    {
       // return $this->render('MobileApiBundle:Default:index.html.twig');
        $em = $this->getDoctrine()->getManager();

        $events = $em->getRepository('GestEventBundle:event')->findAll();
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($events);
        return new JsonResponse($formatted);
      /*  return $this->render('event/index.html.twig', array(
            'events' => $events
        ));   */
    }


    public function userAction()
    {
        // return $this->render('MobileApiBundle:Default:index.html.twig');
        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository('UserBundle:User')->findAll();
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($users);
        return new JsonResponse($formatted);
        /*  return $this->render('event/index.html.twig', array(
              'events' => $events
          ));   */
    }

    public function loginAction($username,$password)
    {
        $formatted=null;
        $user_manager = $this->get('fos_user.user_manager');
        $factory = $this->get('security.encoder_factory');

        $user = $user_manager->findUserByUsername($username);
        if($user==null)
        {
            echo "false";
        }
        else{
            $encoder = $factory->getEncoder($user);

            $bool = ($encoder->isPasswordValid($user->getPassword(),$password,$user->getSalt())) ? $user : false;
            $serializer = new Serializer([new ObjectNormalizer()]);
            $formatted = $serializer->normalize($bool);

        }

        return new JsonResponse($formatted);


       // return array('name' => $bool);
    }


    public function participationAction($idevent,$iduser  )
    {
        $em = $this->getDoctrine()->getManager();
        $check = $em->getRepository('GestEventBundle:ParticipationEvent')->checkPart($idevent , $iduser);
        $event= $em->getRepository('GestEventBundle:event')->find($idevent);
        $user= $em->getRepository('UserBundle:User')->find($iduser);
        if(count($check) > 0)
        {
            $res=false;
            $serializer = new Serializer([new ObjectNormalizer()]);
            $formatted = $serializer->normalize($res);
            return new JsonResponse($formatted);
        }
        else{
            $participationEvent = new ParticipationEvent();

            /* $participationEvent = new Participationevent();
             $form = $this->createForm('GestEventBundle\Form\ParticipationEventType', $participationEvent);
             $form->handleRequest($request);
             $id=$request->query->get('id');
             echo $id;*/

            $participationEvent->setHeure(new \DateTime('now'));
            $participationEvent->setDate(new \DateTime('now'));
            $participationEvent->setEvent_id($event);
            $participationEvent->setUser_id($user);
            $event->setNbr_part($event->getNbrpart()+1);
            //if ($form->isSubmitted() && $form->isValid()) {
            // $participationEvent=$participationEvent->setEvent_id($id);

            $em->persist($participationEvent );
            $em->persist($event );
            $em->flush();
            $res=true;
            $serializer = new Serializer([new ObjectNormalizer()]);
            $formatted = $serializer->normalize($res);
            return new JsonResponse($formatted);
            //}

            /*return $this->render('participationevent/new.html.twig', array(
                'participationEvent' => $participationEvent,
                'form' => $form->createView(),'id'=>$id
            ));*/
        }
    }

    public function myEventAction($iduser)
    {
        $em = $this->getDoctrine()->getManager();
        $participationEvents = $em->getRepository('GestEventBundle:ParticipationEvent')->findUserEvents($iduser);
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formatted = $serializer->normalize($participationEvents);
        return new JsonResponse($formatted);
    }


    public function annulerAction($partid,$eventid)
    {



        if ($partid > 0) {


            $em = $this->getDoctrine()->getManager();
            $part = $em->getRepository('GestEventBundle:ParticipationEvent')->find($partid);
            $event = $em->getRepository('GestEventBundle:event')->find($eventid);
            if (!$part instanceof ParticipationEvent)
                throw $this->createNotFoundException('La page n\'existe pas.');
            $em->remove($part);
            $em->flush($part);
            $event->setNbr_part($part->getEventid()->getNbrpart()-1);
            $em->flush($event);

            $res=true;
            $serializer = new Serializer([new ObjectNormalizer()]);
            $formatted = $serializer->normalize($res);
            return new JsonResponse($formatted);

        } else {
            $res=false;
            $serializer = new Serializer([new ObjectNormalizer()]);
            $formatted = $serializer->normalize($res);
            return new JsonResponse($formatted);
        }

    }


}
