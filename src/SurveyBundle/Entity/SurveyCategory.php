<?php

namespace SurveyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SurveyCategory
 *
 * @ORM\Table(name="survey_category")
 * @ORM\Entity(repositoryClass="SurveyBundle\Repository\SurveyCategoryRepository")
 */
class SurveyCategory
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="Survey", mappedBy="category", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    private $surveis; 

    public function __toString(){
        return $this->getName();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return SurveyCategory
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->surveis = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add survei
     *
     * @param \SurveyBundle\Entity\Survey $survei
     *
     * @return SurveyCategory
     */
    public function addSurvei(\SurveyBundle\Entity\Survey $survei)
    {
        $this->surveis[] = $survei;

        return $this;
    }

    /**
     * Remove survei
     *
     * @param \SurveyBundle\Entity\Survey $survei
     */
    public function removeSurvei(\SurveyBundle\Entity\Survey $survei)
    {
        $this->surveis->removeElement($survei);
    }

    /**
     * Get surveis
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSurveis()
    {
        return $this->surveis;
    }
}
