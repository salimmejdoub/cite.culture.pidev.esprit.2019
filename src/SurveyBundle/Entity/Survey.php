<?php

namespace SurveyBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Survey
 *
 * @ORM\Table(name="survey")
 * @ORM\Entity(repositoryClass="SurveyBundle\Repository\SurveyRepository")
 */
class Survey
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity="Question", mappedBy="survey", cascade={"persist", "remove"})
     * 
     * @ORM\OrderBy({"position"="ASC"})
     */

    protected  $questions; 


    /**
     * @ORM\ManyToOne(targetEntity="SurveyCategory", inversedBy="surveis")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    private $category;




     public function __toString(){
        return $this->getTitle();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Survey
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Survey
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->questions = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add question
     *
     * @param \SurveyBundle\Entity\Question $question
     *
     * @return Survey
     */
    public function addQuestion(\SurveyBundle\Entity\Question $question)
    {
        $question->setSurvey($this);
        $this->questions[] = $question;

        return $this;
    }

    /**
     * Remove question
     *
     * @param \SurveyBundle\Entity\Question $question
     */
    public function removeQuestion(\SurveyBundle\Entity\Question $question)
    {
        $this->questions->removeElement($question);
    }

    /**
     * Get questions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getQuestions()
    {
        return $this->questions;
    }


    /**
     * Set category
     *
     * @param \SurveyBundle\Entity\SurveyCategory $category
     *
     * @return Survey
     */
    public function setCategory(\SurveyBundle\Entity\SurveyCategory $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \SurveyBundle\Entity\SurveyCategory
     */
    public function getCategory()
    {
        return $this->category;
    }
}
