<?php

namespace SurveyBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class QuestionType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
        ->add('content',null,array(
            'attr'=>array(
                'class'=> 'form-control',
                'placeholder'=>'Question',
                'required'=>'required'
            )
        ))
        ->add('position',null,array(
            'label'=>' ',
            'required'=>false,
                'attr'=>array(
                    'class'=>'contentPosition hiddenInput'
                )
            ))
        ->add('answers',CollectionType::class, array(
                'entry_type'   => \SurveyBundle\Form\AnswersType::class,
                'allow_add' => true,
                'delete_empty'=>true,
                'by_reference' => false,
                'allow_delete' => true,
                'prototype' => true,
                'label'=>' ',
                'prototype_name' => '__child_prot__',
                'entry_options'  => array(
                    'attr'      => array('class' => 'answers-box')
                ),
            )
        );
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'SurveyBundle\Entity\Question'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'surveybundle_question';
    }


}
