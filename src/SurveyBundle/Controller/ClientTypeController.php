<?php

namespace SurveyBundle\Controller;

use SurveyBundle\Entity\ClientType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Clienttype controller.
 *
 * @Route("clienttype")
 */
class ClientTypeController extends Controller
{
    private $renderPath = 'SurveyBundle:clientType:';
    /**
     * Lists all clientType entities.
     *
     * @Route("/", name="clienttype_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $clientTypes = $em->getRepository('SurveyBundle:ClientType')->findAll();

        return $this->render($this->renderPath.'index.html.twig', array(
            'clientTypes' => $clientTypes,
        ));
    }

    /**
     * Creates a new clientType entity.
     *
     * @Route("/new", name="clienttype_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $clientType = new Clienttype();
        $form = $this->createForm('SurveyBundle\Form\ClientTypeType', $clientType);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($clientType);
            $em->flush();

            return $this->redirectToRoute('clienttype_edit', array('id' => $clientType->getId()));
        }

        return $this->render($this->renderPath.'new.html.twig', array(
            'clientType' => $clientType,
            'form' => $form->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing clientType entity.
     *
     * @Route("/{id}/edit", name="clienttype_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, ClientType $clientType)
    {
        $deleteForm = $this->createDeleteForm($clientType);
        $editForm = $this->createForm('SurveyBundle\Form\ClientTypeType', $clientType);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('clienttype_edit', array('id' => $clientType->getId()));
        }

        return $this->render($this->renderPath.'edit.html.twig', array(
            'clientType' => $clientType,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a clientType entity.
     *
     * @Route("/{id}", name="clienttype_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, ClientType $clientType)
    {
        $form = $this->createDeleteForm($clientType);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($clientType);
            $em->flush();
        }

        return $this->redirectToRoute('clienttype_index');
    }

    /**
     * Creates a form to delete a clientType entity.
     *
     * @param ClientType $clientType The clientType entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(ClientType $clientType)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('clienttype_delete', array('id' => $clientType->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
