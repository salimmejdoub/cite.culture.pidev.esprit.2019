<?php

namespace SurveyBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session;
use SurveyBundle\Service\SurveyService;


use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Validation;

use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\JsonResponse;

class RoutingController extends Controller
{
	private $renderPath = 'SurveyBundle:frontEnd:';

	/**
     * @Route("/sondage_client/{id}", name="sondage_client")
     * @Method("GET")
     */
    public function sondageClientAction(Request $data, $id)
    {
    	$em = $this->getDoctrine()->getManager();
    	$survey = $em->getRepository('SurveyBundle:Survey')->findOneById( $id );
        return $this->render($this->renderPath.'survey.html.twig', array(
            'survey'=>$survey
        ));
    }

    /**
     * @Route("/all_sondages", name="all_sondages")
     * @Method("GET")
     */
    public function all_sondagesClientAction(Request $httpRequest)
    {
        $em = $this->getDoctrine()->getManager();
        $surveys = $em->getRepository('SurveyBundle:Survey')->findAll();
        return $this->render($this->renderPath.'list.html.twig', array(
            'surveys'=>$surveys
        ));
    }

    /**
     * @Route("/send_sondage_request", name="send_sondage_request")
     * @Method("POST")
     */
    public function sondageClientSendFormAction(Request $httpRequest)
    {
        $dataArray = array();
    	$data = $httpRequest->request;
        $em = $this->getDoctrine()->getManager(); 
        $service = new SurveyService();
        if( $service->validatorBundleFunction($data) == 'validData'  ){
            $state = $service->checkfEmailExist($data->get('email'),$data->get('survey'),$em);
            if(  $state == false ){
                $transporter = \Swift_SmtpTransport::newInstance($this->getParameter('mailer_host'), 587, 'tls')
                ->setUsername($this->getParameter('mailer_user'))
                ->setPassword($this->getParameter('mailer_password'));
                $mailer = \Swift_Mailer::newInstance($transporter);
                $message = (new \Swift_Message('Email de comfirmation de participation'))
                ->setFrom('salim.mejdoub@esprit.tn')
                ->setTo($data->get('email'))
                ->setBody(
                    'Merci Pour Votre participation au sondage Mr/Mme '. $data->get('fname').' '. $data->get('lname')
                );
                $mailer->send($message);
                $service->saveCondidateData($data,$em );
                $dataArray['result'] = 'success' ;
                $dataArray['profile'] = $data->get('fname').' '. $data->get('lname');
            }else{
                $dataArray['result'] = 'error' ;
            }
        }else{
           $dataArray['result'] = 'DataError' ; 
        }
        
        
        return $this->render($this->renderPath.'message.html.twig',$dataArray);
    }

        /**
     * @Route("/all_sondages_json", name="all_sondages_json")
     * @Method("GET")
     */
    public function all_sondagesJsonClientAction(Request $httpRequest)
    {
        header('Access-Control-Allow-Origin: *');
        $em = $this->getDoctrine()->getManager();
        $finalArray = array();
        $service = new SurveyService();
        $surveys = $em->getRepository('SurveyBundle:Survey')->findAll();
        foreach ($surveys as $element) {
            $finalArray[]  = array (
                'name' => $element->getTitle(),
                'id' => $element->getId(),
                'description' => $element->getDescription(),
                'category' => $element->getCategory()->getName(),
                'questions' => $service->getAllQuestions( $element )
            );
        }
        
        return new jsonResponse($finalArray);
    }

        /**
     * @Route("/all_sondages_jsonbyId/{id}", name="all_sondages_jsonbyId")
     * @Method("GET")
     */
    public function all_sondagesJsonClientByIdAction(Request $httpRequest,$id)
    {
        header('Access-Control-Allow-Origin: *');
        //dump($id);die;
        $em = $this->getDoctrine()->getManager();
        $finalArray = array();
        $service = new SurveyService();
        $element = $em->getRepository('SurveyBundle:Survey')->findOneById($id);
        $finalArray[]  = array (
            'name' => $element->getTitle(),
            'id' => $element->getId(),
            'description' => $element->getDescription(),
            'category' => $element->getCategory()->getName(),
            'questions' => $service->getAllQuestions( $element )
        );
        return new jsonResponse($finalArray);
    }
}
