<?php

namespace MainBundle\Entity;

use ContainerVmyeruh\appDevDebugProjectContainer;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * MediaP
 *
 * @ORM\Table(name="mediap")
 * @ORM\Entity(repositoryClass="MainBundle\Repository\MediaPRepository")
 */
class MediaP
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="insertat", type="datetime", nullable=true)
     */
    private $insertat;

    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=255, nullable=true)
     */
    private $path;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return MediaP
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set insertat
     *
     * @param \DateTime $insertat
     *
     * @return MediaP
     */
    public function setInsertat($insertat)
    {
        $this->insertat = $insertat;

        return $this;
    }

    /**
     * Get insertat
     *
     * @return \DateTime
     */
    public function getInsertat()
    {
        return $this->insertat;
    }

    /**
     * Set path
     *
     * @param string $path
     *
     * @return MediaP
     */
    public function setPath($path)
    {
        $this->path = $path;

        return $this;
    }

    /**
     * Get path
     *
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }
}
