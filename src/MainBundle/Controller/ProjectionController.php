<?php

namespace MainBundle\Controller;

use MainBundle\Entity\MediaP;
use MainBundle\Entity\Projection;
use MainBundle\Entity\Salle;
use MainBundle\Form\ProjectionType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class ProjectionController extends Controller
{
    public function createAction(Request $request)
    {// creation d'une formulaire
        //prepatation d'une instance
        $Projection = new Projection();
        //preparation de notre formulaire
        $forum = $this->createForm(ProjectionType::class, $Projection);
        //recuperation des donnees a partir de formulaire
        $forum->handleRequest($request);
        if ($forum->isValid()) {
            $file = $Projection->getFile();
            if ($file) {
                // generation d'un path unique et automatique de l'image
                $fileName = md5(uniqid()) . '.' . $file->guessExtension();

                //insertion de l'image sous le dossier web/uploads
                $file->move($this->getParameter('upload_directory_p'), $fileName);

                $mediap = new MediaP();

                $mediap->setPath($fileName);
                $mediap->setName($Projection->getImageFileName());

                //insertion date d'upload
                $mediap->setInsertat(new \DateTime());

                $em = $this->getDoctrine()->getManager();
                $em->persist($mediap);
                $em->flush($mediap);
                if ($mediap->getId() > 0) {
                    $Projection->setImage($mediap);
                }
            }

            //insertion dans la base
            $em = $this->getDoctrine()->getManager();
            $Projection->getSalle()->setEtatSalle('NON DISPONIBLE');
            $em->persist($Projection);
            $em->flush();
            return $this->redirectToRoute("read_projection");
        }
        $salled = $this->getDoctrine()->getRepository(Projection::class)->findByXY();
        return $this->render('@Main/Projection/create.html.twig', array(
            'forum' => $forum->createView(), 'salleed' => $salled));
    }

    public function readAction()
    {
        $Projection = $this->getDoctrine()
            ->getRepository(Projection::class)
            ->findall();
        return $this->render('@Main/Projection/read.html.twig', array(
            'Projection' => $Projection
        ));
    }

    public function updateAction($id, Request $request)
    {   // recupeation des données a partir de base
        $em = $this->getDoctrine()->getManager();
        $Projection = $em->getRepository(Projection::class)->find($id);
        // preparation du formulaire
        $form = $this->createForm(ProjectionType::class, $Projection);
        $form = $form->handleRequest($request);
        if ($form->isValid()) {
            $em->flush();
            return $this->redirectToRoute("read_projection");
        }
        return $this->render('@Main/Projection/update.html.twig', array(
            'forum' => $form->createView()));
    }

    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $Projection = $em->getRepository(Projection::class)->find($id);
        $em->remove($Projection);
        $em->flush();
        return $this->redirectToRoute("read_projection");

    }

    public function uploadPAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $projection = $em->getRepository('MainBundle:Projection')->find($id);
        if (!$projection instanceof Projection) {
            throw $this->createNotFoundException('La page n\'existe pas.');
        }

        //creation d'un form
        $mediap = new MediaP();
        $form = $this->createForm('MainBundle\Form\MediaPType', $mediap);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $file = $mediap->getFile();

            // generation d'un path unique et automatique de l'image
            $fileName = md5(uniqid()) . '.' . $file->guessExtension();

            //insertion de l'image sous le dossier web/uploads
            $file->move($this->getParameter('upload_directory'), $fileName);

            $mediap->setPath($fileName);
            $mediap->setProjection($projection);

            //insertion date d'upload
            $mediap->setInsertat(new \DateTime());

            $em = $this->getDoctrine()->getManager();
            $em->persist($mediap);
            $em->flush($mediap);
            return $this->redirectToRoute('Projection_upload', array('id' => $id));
        }

        return $this->render('@Main/Projection/uploadP.html.twig', array(
            'form' => $form->createView(),
        ));
    }


}
