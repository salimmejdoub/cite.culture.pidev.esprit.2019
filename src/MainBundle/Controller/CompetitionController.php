<?php

namespace MainBundle\Controller;

use MainBundle\Entity\Competition;
use MainBundle\Entity\CompetitionUser;
use MainBundle\Entity\Media;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;

class CompetitionController extends Controller
{

    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $competitions = $em->getRepository('MainBundle:Competition')->findAll();
        return $this->render('@Main/competition/index.html.twig', array(
            'competitions' => $competitions
        ));
    }

    public function newAction(Request $request)
    {
        $competition = new Competition();
        $form = $this->createForm('MainBundle\Form\CompetitionType', $competition);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $file = $competition->getFile();
            if ($file instanceof UploadedFile) {
                // generation d'un path unique et automatique de l'image
                $fileName = md5(uniqid()) . '.' . $file->guessExtension();

                //insertion de l'image sous le dossier web/uploads
                $file->move($this->getParameter('upload_directory'), $fileName);

                $media = new Media();
                $media->setPath($fileName);
                $media->setName($file->getClientOriginalName());
                //insertion date d'upload
                $media->setInsertat(new \DateTime());

                $em = $this->getDoctrine()->getManager();
                $em->persist($media);
                $em->flush($media);
            }

            if ($media->getId() > 0) {
                $competition->setMedia($media);
            }

            $competition->setInsertat(new \DateTime());
            $em = $this->getDoctrine()->getManager();
            $em->persist($competition);
            $em->flush($competition);

            return $this->redirectToRoute('competition_index');
        }

        return $this->render('@Main/Competition/new.html.twig', array(
            'competition' => $competition,
            'form' => $form->createView(),
        ));
    }

    public function editAction($id, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $competition = $em->getRepository('MainBundle:Competition')->find($id);
        if (!$competition instanceof Competition)
            throw $this->createNotFoundException('La page n\'existe pas.');
        $editform = $this->createForm('MainBundle\Form\CompetitionType', $competition);
        $editform->handleRequest($request);

        if ($editform->isSubmitted() && $editform->isValid()) {

            $file = $competition->getFile();
            if ($file instanceof UploadedFile) {
                if ($this->mediadelete($competition->getMedia())) {

                    // generation d'un path unique et automatique de l'image
                    $fileName = md5(uniqid()) . '.' . $file->guessExtension();

                    //insertion de l'image sous le dossier web/uploads
                    $file->move($this->getParameter('upload_directory'), $fileName);

                    $media = $competition->getMedia();
                    $media->setPath($fileName);
                    $media->setName($file->getClientOriginalName());
                    //insertion date d'upload
                    $media->setInsertat(new \DateTime());

                    $em = $this->getDoctrine()->getManager();
                    $em->persist($media);
                    $em->flush($media);
                }

                if ($media->getId() > 0) {
                    $competition->setMedia($media);
                }
            }

            $em->flush($competition);

            return $this->redirectToRoute('competition_index');
        }
        return $this->render('@Main/Competition/edit.html.twig', array(
            'competition' => $competition,
            'form' => $editform->createView(),
        ));


    }

    protected function mediadelete($media)
    {
        if (file_exists($this->getParameter('upload_directory') . '/' . $media->getPath())) {
            unlink($this->getParameter('upload_directory') . '/' . $media->getPath());
        } else {
            throw $this->createNotFoundException('La page n\'existe pas.');
        }

        return true;
    }

    public function deleteAction($id)
    {
        if ($id > 0) {
            $em = $this->getDoctrine()->getManager();
            $competition = $em->getRepository('MainBundle:Competition')->find($id);
            if (!$competition instanceof Competition)
                throw $this->createNotFoundException('La page n\'existe pas.');


            $em->remove($competition);
            $em->flush($competition);
            return $this->redirectToRoute('competition_index');

        } else {
            throw $this->createNotFoundException('La page n\'existe pas.');
        }

    }

    public function participationAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $competition = $em->getRepository('MainBundle:Competition')->find($id);
        if (!$competition instanceof Competition)
            throw $this->createNotFoundException('La page n\'existe pas.');
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if ($competition->getClub()->getUser() != $user)
            throw new AccessDeniedExceptio();

        $demandes = $em->getRepository('MainBundle:CompetitionUser')->findBy(
            array('competition' => $id)
        );
        return $this->render('@Main/Competition/listParticipation.html.twig', array(
            'demandes' => $demandes,
        ));
    }

    public function accepterDemandeAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $demande = $em->getRepository('MainBundle:CompetitionUser')->find($id);
        if (!$demande instanceof CompetitionUser)
            throw $this->createNotFoundException('La page n\'existe pas.');
        // droits d acces administrateur
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if ($demande->getCompetition()->getClub()->getUser() != $user)
            throw new AccessDeniedException();

        $demande->setStatus(1);
        $em->persist($demande);
        $em->flush($demande);

        if ($this->sendEmail($demande, $user)) {
            $this->get('session')->getFlashbag()->add('success', 'Demande acceptée avec succés, un email de confirmation a été envoyé au client pour l\'informer au details d\'inscription.');
        } else {
            $this->get('session')->getFlashbag()->add('error', 'Erreur survenue au court d\'envoie d\'Email de confirmation au client.' );
        }

        return $this->redirectToRoute('competition_list_participation', array('id' => $demande->getCompetition()->getId()));
    }

    public function refuserDemandeAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $demande = $em->getRepository('MainBundle:CompetitionUser')->find($id);
        if (!$demande instanceof CompetitionUser)
            throw $this->createNotFoundException('La page n\'existe pas.');
        // droits d acces administrateur
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if ($demande->getCompetition()->getClub()->getUser() != $user)
            throw new AccessDeniedException();

        $demande->setStatus(2);
        $em->persist($demande);
        $em->flush($demande);

        if ($this->sendEmail($demande, $user)) {
            $this->get('session')->getFlashbag()->add('success', 'Demande refusé avec succés, un email de refus a été envoyé au client');
        } else {
            $this->get('session')->getFlashbag()->add('error', 'Erreur survenue au court d\'envoie d\'Email de confirmation au client.' );
        }

        return $this->redirectToRoute('competition_list_participation', array('id' => $demande->getCompetition()->getId()));
    }

    protected function sendEmail($demande, $user)
    {
        $transporter = \Swift_SmtpTransport::newInstance($this->getParameter('mailer_host'), 587, 'tls')
            ->setUsername($this->getParameter('mailer_user'))
            ->setPassword($this->getParameter('mailer_password'));

        $mailer = \Swift_Mailer::newInstance($transporter);

        if ($demande->getStatus() == 1) {
            $template = '@Main/Competition/templateEmail/confirmationParticipation.html.twig';
        }
        if ($demande->getStatus() == 2) {
            $template = '@Main/Competition/templateEmail/refusParticipation.html.twig';
        }

        $message = (new \Swift_Message('Email de comfirmation de participation'))
            ->setFrom('zeineb.haouari@esprit.tn')
            ->setTo('zeineb.haouari@esprit.tn')
            ->addCc($demande->getUser()->getEmail())
            ->setBody(
                $this->renderView(
                    $template, array('demande' => $demande, 'user' => $user)
                ), 'text/html'
            );
        return $mailer->send($message);
    }
}
