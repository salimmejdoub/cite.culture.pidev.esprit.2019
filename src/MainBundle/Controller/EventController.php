<?php

namespace MainBundle\Controller;

use MainBundle\Entity\Event;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


/**
 * Event controller.
 *
 */
class EventController extends Controller
{
    /**
     * Lists all event entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $events = $em->getRepository('MainBundle:Event')->findAll();

        return $this->render('event/index.html.twig', array(
            'events' => $events,
        ));
    }

    /**
     * Finds and displays a event entity.
     *
     */
    public function showAction(Event $event)
    {

        return $this->render('event/show.html.twig', array(
            'event' => $event,
        ));
    }
}
