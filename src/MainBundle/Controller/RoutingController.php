<?php

namespace MainBundle\Controller;

use FOS\UserBundle\FOSUserBundle;
use MainBundle\Entity\Salle;
use MainBundle\Form\ProjectionType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use MainBundle\Entity\Projection;
use UserBundle\Entity\User;
use Symfony\Component\HttpFoundation\Response;
use UserBundle\UserBundle;
class RoutingController extends Controller
{

    public function ListeProjectionAction()
    {
        $Projection = $this->getDoctrine()
            ->getRepository(Projection::class)
            ->findall();
        return $this->render('@Main/Route/read.html.twig', array(
            'Projection' => $Projection
        ));
    }
    public function rechercheAction(request $request)
    {
        $recherche = $request->request->get("recherche");

        $Projection=$this->getDoctrine()->getRepository(Projection::class)->myfindall($recherche);

            return $this->render('@Main/Route/read.html.twig', array(
                'Projection' => $Projection
            ));

        }



    public function listeAction(Request $request, $id)
    {
        $Projection = $this->getDoctrine()
            ->getRepository(Projection::class)
            ->find($id);
        return $this->render('@Main/Route/detailsProjection.html.twig', array(
            'Projection' => $Projection
        ));
    }

    public function decrementAction(Request $request, $id)
    {
        if ($request->isMethod('POST')) {
            if ($request->get('decrement') == "decrement") {
                $number = $request->get('number');
                $Salle = $this->getDoctrine()->getRepository(Salle::class)->find($id);
                $n = $Salle->getNbreChaiseSalle() - $number;
                $Salle->setNbreChaiseSalle($n);
                $em = $this->getDoctrine()->getManager();
                $em->persist($Salle);
                $em->flush();


            }
        }

        $title = $request->request->get("title");
        $user= $request->request->get("user");
        $date=$request->request->get("date");
        $Nom=$request->request->get("Nom");
        $Prenom=$request->request->get("Prenom");
        $description=$request->request->get("description");
        $prix=$request->request->get("prix");


       $snappy= $this->get('knp_snappy.pdf');
        $html=$this->renderView(
                '@Main/Route/vide.html.twig',
                array(

                    'title' => $title,
                    'user' => $user,
                    'date' => $date,
                    'Nom' => $Nom,
                    'Prenom' => $Prenom,
                    'description' => $description,
                    'prix' => $prix,
                    'number' => $number,



                )


        );
        $filename = 'myFirstSnappyPDF';
        $snappy = $this->get('knp_snappy.pdf');
        return new Response(
            $snappy->getOutputFromHtml($html),
            200,
            array(
                'Content-Type'          => 'application/pdf',
                'Content-Disposition'   => 'inline; filename="'.$filename.'.pdf"'
            )
        );
    }

    public function dispoAction($id)
    {
        $Projection = $this->getDoctrine()->getRepository(Projection::class)->find($id);
        $Projection->getSalle()->setEtatSalle('NON DISPONIBLE');
        $em = $this->getDoctrine()->getManager();
        $em->persist($Projection);
        $em->flush();
        return $this->render('@Main/Projection/create.html.twig');

    }

    public function chercherAction(Request $request)
    {
        $Projection = new Projection();
        $form = $this->createFormBuilder($Projection)
            ->add('title', TextType::class)
            ->getForm();
        $form->handleRequest($request);
        if ($form->isValid()) {
            $resultCapteur = $this->getDoctrine()->getRepository(Projection::class)->findByXY($Projection->getTitleProjection());
            return $this->render('@Main/Route/read.html.twig', array('form' => $form->createView(), 'capteur' => $resultCapteur));


        }
    }

}
