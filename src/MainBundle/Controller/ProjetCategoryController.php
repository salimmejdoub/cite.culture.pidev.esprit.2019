<?php

namespace MainBundle\Controller;

use MainBundle\Entity\ProjetCategory;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Projetcategory controller.
 *
 */
class ProjetCategoryController extends Controller
{
    /**
     * Lists all projetCategory entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $projetCategories = $em->getRepository('MainBundle:ProjetCategory')->findAll();

        return $this->render('projetcategory/index.html.twig', array(
            'projetCategories' => $projetCategories,
        ));
    }

    /**
     * Creates a new projetCategory entity.
     *
     */
    public function newAction(Request $request)
    {
        $projetCategory = new Projetcategory();
        $form = $this->createForm('MainBundle\Form\ProjetCategoryType', $projetCategory);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($projetCategory);
            $em->flush();

            return $this->redirectToRoute('projetcategory_edit', array('id' => $projetCategory->getId()));
        }

        return $this->render('projetcategory/new.html.twig', array(
            'projetCategory' => $projetCategory,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a projetCategory entity.
     *
     */
    public function showAction(ProjetCategory $projetCategory)
    {
        $deleteForm = $this->createDeleteForm($projetCategory);

        return $this->render('projetcategory/show.html.twig', array(
            'projetCategory' => $projetCategory,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing projetCategory entity.
     *
     */
    public function editAction(Request $request, ProjetCategory $projetCategory)
    {
        $deleteForm = $this->createDeleteForm($projetCategory);
        $editForm = $this->createForm('MainBundle\Form\ProjetCategoryType', $projetCategory);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('projetcategory_edit', array('id' => $projetCategory->getId()));
        }

        return $this->render('projetcategory/edit.html.twig', array(
            'projetCategory' => $projetCategory,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a projetCategory entity.
     *
     */
    public function deleteAction(Request $request, ProjetCategory $projetCategory)
    {
        $form = $this->createDeleteForm($projetCategory);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($projetCategory);
            $em->flush();
        }

        return $this->redirectToRoute('projetcategory_index');
    }

    /**
     * Creates a form to delete a projetCategory entity.
     *
     * @param ProjetCategory $projetCategory The projetCategory entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(ProjetCategory $projetCategory)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('projetcategory_delete', array('id' => $projetCategory->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
