<?php

namespace MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Projet
 *
 * @ORM\Table(name="projet")
 * @ORM\Entity(repositoryClass="MainBundle\Repository\ProjetRepository")
 */
class Projet
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titreProjet", type="string", length=255)
     */
    private $titreProjet;

    /**
     * @var string
     *
     * @ORM\Column(name="descriptionProjet", type="string", length=255)
     */
    private $descriptionProjet;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="dateSoumission", type="date")
     */
    private $dateSoumission;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="echeanceProjet", type="date")
     */
    private $echeanceProjet;

    /**
     * @var string
     *
     * @ORM\Column(name="exigenceProjet", type="string", length=255)
     */
    private $exigenceProjet;

    /**
     * @var string
     *
     * @ORM\Column(name="cibleProjet", type="string", length=255)
     */
    private $cibleProjet;

    /**
     * @ORM\ManyToOne(targetEntity="ProjetCategory", inversedBy="projets")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    private $category;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set titreProjet
     *
     * @param string $titreProjet
     *
     * @return Projet
     */
    public function setTitreProjet($titreProjet)
    {
        $this->titreProjet = $titreProjet;

        return $this;
    }

    /**
     * Get titreProjet
     *
     * @return string
     */
    public function getTitreProjet()
    {
        return $this->titreProjet;
    }

    /**
     * Set descriptionProjet
     *
     * @param string $descriptionProjet
     *
     * @return Projet
     */
    public function setDescriptionProjet($descriptionProjet)
    {
        $this->descriptionProjet = $descriptionProjet;

        return $this;
    }

    /**
     * Get descriptionProjet
     *
     * @return string
     */
    public function getDescriptionProjet()
    {
        return $this->descriptionProjet;
    }

    /**
     * Set dateSoumission
     *
     * @param \DateTime $dateSoumission
     *
     * @return Projet
     */
    public function setDateSoumission($dateSoumission)
    {
        $this->dateSoumission = $dateSoumission;

        return $this;
    }

    /**
     * Get dateSoumission
     *
     * @return \DateTime
     */
    public function getDateSoumission()
    {
        return $this->dateSoumission;
    }

    /**
     * Set echeanceProjet
     *
     * @param \DateTime $echeanceProjet
     *
     * @return Projet
     */
    public function setEcheanceProjet($echeanceProjet)
    {
        $this->echeanceProjet = $echeanceProjet;

        return $this;
    }

    /**
     * Get echeanceProjet
     *
     * @return \DateTime
     */
    public function getEcheanceProjet()
    {
        return $this->echeanceProjet;
    }

    /**
     * Set exigenceProjet
     *
     * @param string $exigenceProjet
     *
     * @return Projet
     */
    public function setExigenceProjet($exigenceProjet)
    {
        $this->exigenceProjet = $exigenceProjet;

        return $this;
    }

    /**
     * Get exigenceProjet
     *
     * @return string
     */
    public function getExigenceProjet()
    {
        return $this->exigenceProjet;
    }

    /**
     * Set cibleProjet
     *
     * @param string $cibleProjet
     *
     * @return Projet
     */
    public function setCibleProjet($cibleProjet)
    {
        $this->cibleProjet = $cibleProjet;

        return $this;
    }

    /**
     * Get cibleProjet
     *
     * @return string
     */
    public function getCibleProjet()
    {
        return $this->cibleProjet;
    }

    /**
     * Set category
     *
     * @param \MainBundle\Entity\ProjetCategory $category
     *
     * @return Projet
     */
    public function setCategory(\MainBundle\Entity\ProjetCategory $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \MainBundle\Entity\ProjetCategory
     */
    public function getCategory()
    {
        return $this->category;
    }
}
