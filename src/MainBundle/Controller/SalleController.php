<?php

namespace MainBundle\Controller;

use MainBundle\Entity\Projection;
use MainBundle\Entity\Salle;
use MainBundle\Form\ProjectionType;
use MainBundle\Form\SalleType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Translation\Dumper\QtFileDumper;

class SalleController extends Controller
{
    public function  createAction(Request $request)
    {// creation d'une formulaire
        //prepatation d'une instance
        $Salle=new Salle();
        //preparation de notre formulaire
        $forum=$this->createForm(SalleType::class,$Salle);
        //recuperation des donnees a partir de formulaire
        $forum=$forum->handleRequest($request);
        if ($forum->isValid())
        {
            //insertion dans la base
            $em=$this->getDoctrine()->getManager();
            $em->persist($Salle);
            $em->flush();
            return $this->redirectToRoute("read_salle");

        }
        //envoi de notre formulaire au view

        return $this->render('@Main/Salle/create.html.twig', array(
            'forum'=>$forum->createView()));
    }
    public function readAction()
    { $Salle=$this->getDoctrine()
        ->getRepository(Salle::class)
        ->findall();
    //dump($Salle);die;
        return $this->render('@Main/Salle/read.html.twig', array(
            'Salle'=>$Salle
        ));
    }
    public function updateAction($id, Request $request)
    {   // recupeation des données a partir de base
        $em=$this->getDoctrine()->getManager();
        $Salle=$em->getRepository(Salle::class)->find($id);
        // preparation du formulaire
        $form=$this->createForm(SalleType::class,$Salle);
        $form=$form->handleRequest($request);
        if($form->isValid())
        {$em->flush();
            return $this->redirectToRoute("read_salle");
        }
        return $this->render('@Main/Salle/update.html.twig', array(
            'forum'=>$form->createView()));
    }
    public function deleteAction ($id)
    { $em=$this->getDoctrine()->getManager();
        $Salle=$em->getRepository(Salle::class)->find($id);
        $em->remove($Salle);
        $em->flush();
        return $this->redirectToRoute("read_salle");

    }
    public function rechercheAction(Request $request)
    {
        $Capteur = $this->getDoctrine()
            ->getRepository(Salle::class)
            ->findByXY($request->get("x"), $request->get("y"));
        return $this->render('@Main/Salle/recherche.html.twig', array(
            'Capteur' => $Capteur
        ));
    }
     public function filter()
     {
    $salled=$this->getDoctrine()
        ->getRepository(Salle::class)
        ->findByXY();
    return $this->render('@Main/Projection/create.html.twig', array(
        'salled' => $salled
    ));
    }


        }






