<?php

namespace MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use MainBundle\Entity\Projet;
use Symfony\Component\HttpFoundation\Request;
use MainBundle\Form\ProjetType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use CircularReferenceLimit;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
class ProjetController extends Controller
{

    public function indexeAction()
    {
        return $this->render('@Main/default/projet.html.twig');
    }

    public function readAction(Request $request)
    {


        $em = $this->getDoctrine()->getManager();
        $projets = $em->getRepository(projet::class)->findAll();
        $projet  = $this->get('knp_paginator')->paginate(
            $projets,
            $request->query->get('page', 1)/*le numéro de la page à afficher*/,
            2/*nbre d'éléments par page*/
        );
        return $this->render('@Main/default/projetread.html.twig', array(
            'projet' => $projet,));
    }

    public function createAction(Request $request)
    {
        //0-creation de notre formulaire (avec sdoctrinegenerateform EspritPardBundle:Modele)
        //1.a - preparation d'un instance
        $projet=new Projet();
        //1.b -preparation de notre formulaire
        $form=$this->createForm(ProjetType::class,$projet);
        //2-recuperation des données
        $form=$form->handleRequest($request);
        if ($form->isValid()){
            //3-insertion dans la base
            $em=$this->getDoctrine()->getManager();
            $em->persist($projet);
            $em->flush();
            return $this->redirectToRoute('read_project');
            //return $this->redirect('http://localhost/pidev-iskanderChikhaoui2/web/app_dev.php/main/projetread');
        }
        //1.c -l'envoi de notre formulaire au view

        return $this->render('@Main/default/create.html.twig', array(
            'form'=>$form->createView()
        ));

    }

    public function updateAction($id,Request $request)
    {
        //1-recuperation de l'objet
        $em=$this->getDoctrine()->getManager();
        $projet=$em->getRepository(Projet::class)->find($id);
        //2-preparation de formulaire
        $form=$this->createForm(ProjetType::class,$projet);
        $form=$form->handleRequest($request);
        if ($form->isValid()){
            $em->flush();
            return $this->redirectToRoute("read_project");
        }
        return $this->render('@Main/default/maj.html.twig', array(
            'form'=>$form->createView()
        ));

    }


    public function deleteAction($id)
    {
        //recuperation de l'objet
        $em=$this->getDoctrine()->getManager();
        $projet=$em->getRepository(Projet::class)->find($id);
        $em->remove($projet);
        $em->flush();
        return $this->redirectToRoute('read_project');

    }

    public function indexesAction()
    {
        return $this->render('@Main/Default/base2.html.twig');
    }


    public function readeAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $projets = $em->getRepository(projet::class)->findAll();




        $projet  = $this->get('knp_paginator')->paginate(
            $projets,
            $request->query->get('page', 1)/*le numéro de la page à afficher*/,
            2/*nbre d'éléments par page*/
        );
        return $this->render('@Main/default/index2.html.twig', array(
            'projet' => $projet,));
    }
    public function deleteCommentAction( Request $request, $id ){
        $em = $this->getDoctrine()->getManager();
        $parentComment = $em->getRepository('MainBundle:Comment')->findOneById($id);
        $childrenComments = $em->getRepository('MainBundle:Comment')->findBy(
            array( 'ancestors'=> $id ),
            array()
        );
        if( sizeof( $childrenComments ) > 0 ){
            foreach ($childrenComments as $el){
                $em->remove($el);
                $em->flush();
            }
        }
        $em->remove($parentComment);
        $em->flush();
        return new JsonResponse(array('hello'));
    }
    public function pdfAction( Request $request, $test,$desc,$username,$date)
    {
        $snappy = $this->get('knp_snappy.pdf');

        $html = $this->renderView('@Main/Default/pdf.html.twig',array(
            'test' => $test,
            'desc' => $desc,
            'username' => $username,
            'date'=> $date
        ) );

        $filename = 'myFirstSnappyPDF';

        return new Response(
            $snappy->getOutputFromHtml($html),
            200,
            array(
                'Content-Type'          => 'application/pdf',
                'Content-Disposition'   => 'inline; filename="'.$filename.'.pdf"'
            )
        );
    }

    public function detailAction(Request $request,$titre,$desc ,$date)
    {
        return $this->render('@Main/Default/detailProjet.html.twig', array('test'=>$titre , 'desc'=>$desc ,
         'date'=>$date));

    }


    public function allAction(){

        $projets = $this->getDoctrine()->getManager()->getRepository('MainBundle:Projet')
            ->findAll();
//        $serializer = new Serializer([new ObjectNormalizer()]);
//        CircularReferenceLimit::setCircularReferenceLimit(2);
//        $formatted = $serializer->normalize($projet);

        $normalizer = new ObjectNormalizer();
        $normalizer->setCircularReferenceLimit(2);
        // Add Circular reference handler
        $normalizer->setCircularReferenceHandler(function ($object) {
        return $object->getId();
        });
        $normalizers = array($normalizer);
        $serializer = new Serializer($normalizers, [new JsonEncoder()]);
        $formatted = $serializer->normalize($projets);



        return new JsonResponse($formatted);
    }

}
