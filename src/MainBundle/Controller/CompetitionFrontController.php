<?php

namespace MainBundle\Controller;

use Http\Adapter\Guzzle6\Client;
use Http\Message\MessageFactory\GuzzleMessageFactory;
use Ivory\GoogleMap\Base\Coordinate;
use Ivory\GoogleMap\Map;
use Ivory\GoogleMap\Service\Geocoder\GeocoderService;
use Ivory\GoogleMap\Service\Geocoder\Request\GeocoderAddressRequest;
use MainBundle\Entity\CompetitionUser;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use UserBundle\Entity\User;

class CompetitionFrontController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $competitions = $em->getRepository('MainBundle:Competition')->findAll();
        return $this->render('@Main/competitionfront/index.html.twig', array(
            'competitions' => $competitions
        ));
    }

    public function showAction($id)
    {
        if ($id > 0) {
            $em = $this->getDoctrine()->getManager();
            $competition = $em->getRepository('MainBundle:Competition')->find($id);
            $user = $this->get('security.token_storage')->getToken()->getUser();
            $demande = $em->getRepository('MainBundle:CompetitionUser')->findOneBy(
                array('user' => $user->getId(), 'competition' => $competition->getId())
            );
            return $this->render('@Main/competitionfront/detailsCompetition.html.twig', array(
                'competition' => $competition,
                'demande' => $demande
            ));
        }
    }

    public function participerAction($id)
    {
        $user = $this->get('security.token_storage')->getToken()->getUser();
        if ($id > 0 && $user instanceof User) {
            $em = $this->getDoctrine()->getManager();
            $competition = $em->getRepository('MainBundle:Competition')->find($id);
            $demande = new CompetitionUser();
            $demande->setCompetition($competition);
            $demande->setUser($user);
            $demande->setInsertat(new \DateTime());
            $code = sha1(uniqid());
            $demande->setCode($code);
            $demande->setStatus(0);
            $em->persist($demande);
            $em->flush($demande);
            $this->get('session')->getFlashbag()->add('success','Votre demande de participation a la compétition à été envoyé avec succés, vous recevez un email de confirmation avant la date de fermeture d\'inscription.');
            return $this->redirectToRoute('competition_front_details', array('id' => $competition->getId()));
        } else {
            throw $this->createNotFoundException('La page n\'existe pas.');
        }
    }
}
