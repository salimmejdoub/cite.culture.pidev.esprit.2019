<?php

namespace MainBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;


class SalleType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nomSalle',null,array(
                        'attr'=>array(
                            'class'=> 'form-control',
                            'placeholder'=>'nomSalle'
                )
            ))
            ->add('blocSalle',ChoiceType::class,
                array(
                    "choices"=>array(
                        ""=>"",
                        "BLOC A"=>"BLOC A",
                        "BLOC B"=>"BLOC B",
                        "BLOC C"=>"BLOC C"

                    ),

               'attr'=>array(
                    'class'=> 'form-control',
                    'placeholder'=>'blocSalle'
                     )
                 ))
            ->add('nbreChaiseSalle',null,array(
                'attr'=>array(
                    'class'=> 'form-control',
                    'placeholder'=>'nbreChaiseSalle'
                )
            ))
            ->add('categorieSalle',ChoiceType::class,
                array(
                    "choices"=>array(
                        ""=>"",
                        "Salle_Cinema"=>"Salle_Cinema",
                        "Salle_Théatre"=>"Salle_Théatre"
                    ),
                    'attr'=>array(
                        'class'=> 'form-control',
                        'placeholder'=>'categorieSalle'
                    )
            ))
            ->add('Etat_salle',ChoiceType::class,
                array(
                    "choices"=>array(
                        ""=>"",
                        "DISPONIBLE"=>"DISPONIBLE",
                        "NON DISPONIBLE"=>"NON DISPONIBLE"
                    ),
                'attr'=>array(
                    'class'=> 'form-control',
                    'placeholder'=>'Etat_salle',

                )
            ))
            ->add('Ajouter',SubmitType::class);
    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'MainBundle\Entity\Salle'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'mainbundle_salle';
    }


}
