<?php
// src/UserBundle/Entity/User.php

namespace UserBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    public function __construct()
    {
        parent::__construct();
        // your own logic
    }

    /**
     * @ORM\Column(name="nom", type="string", length=255,nullable=true)
     *
     */
    private $nom;

    /**
     * @ORM\Column(name="prenom", type="string", length=255,nullable=true)
     *
     */
    private $prenom;
	
	
	    /**
     * @ORM\OneToMany(targetEntity="\MainBundle\Entity\ClubVoucher", mappedBy="user", cascade={"remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $vouchers;

    /**
     * @ORM\OneToMany(targetEntity="\MainBundle\Entity\CompetitionUser", mappedBy="user", cascade={"remove"})
     * @ORM\JoinColumn(nullable=true)
     */
    private $demandes;

    /**
     * Add demande
     *
     * @param \MainBundle\Entity\CompetitionUser $demande
     *
     * @return User
     */
    public function addDemandeCompetition(\MainBundle\Entity\CompetitionUser $demande)
    {
        $this->demandes[] = $demande;

        return $this;
    }

    /**
     * Remove demande
     *
     * @param \MainBundle\Entity\CompetitionUser $demande
     */
    public function removeDemandeCompetition(\MainBundle\Entity\CompetitionUser $demande)
    {
        $this->demandes->removeElement($demande);
    }

    /**
     * Get demandes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getDemandesCompetitions()
    {
        return $this->demandes;
    }

    /**
     * Add voucher
     *
     * @param \MainBundle\Entity\ClubVoucher $voucher
     *
     * @return User
     */
    public function addVoucher(\MainBundle\Entity\ClubVoucher $voucher)
    {
        $this->vouchers[] = $voucher;

        return $this;
    }

    /**
     * Remove voucher
     *
     * @param \MainBundle\Entity\ClubVoucher $voucher
     */
    public function removeVoucher(\MainBundle\Entity\ClubVoucher $voucher)
    {
        $this->vouchers->removeElement($voucher);
    }

    /**
     * Get vouchers
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getVouchers()
    {
        return $this->vouchers;
    }


    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }


    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }



    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }



    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @param mixed $nom
     */
    public function setNom($nom)
    {
        $this->nom = $nom;
    }

    /**
     * @return mixed
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * @param mixed $prenom
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;
    }
	
    public function getFullname(){
        return $this->nom." ".$this->prenom;
    }


}
