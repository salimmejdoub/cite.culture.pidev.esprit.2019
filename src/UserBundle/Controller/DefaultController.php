<?php

namespace UserBundle\Controller;

use GestEventBundle\Entity\event;
use GestEventBundle\Entity\ParticipationEvent;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Entity\User;

class DefaultController extends Controller
{
    public function indexAction()
    { $em = $this->getDoctrine()->getManager();

    $users = $this->getUser();
        $notification=$em->getRepository('GestEventBundle:Notification')->findAll();
        $events = $em->getRepository('GestEventBundle:event')->findAll();
        return $this->render('UserBundle:Default:index.html.twig', array(
           'users' => $users , 'events' => $events , 'notification'=>$notification,
        ));
    }

    public function index2Action()
    {
        $em = $this->getDoctrine()->getManager();

        $events = $em->getRepository('GestEventBundle:event')->findAll();

        return $this->render('UserBundle:Default:index.html.twig', array(
            'events' => $events
        ));
    }

    public function myEventAction(User $user)
    {
        $em = $this->getDoctrine()->getManager();
        echo $user->getId();
        $participationEvents = $em->getRepository('UserBundle:User')->find();
        return $this->render('UserBundle:Default:UserEvents.html.twig', array(
            'participationEvents' => $participationEvents,
        ));

        /*$em = $this->getDoctrine()->getManager();
        $id_user=$request->query->get('id');
        $participationEvents = $em->getRepository('GestEventBundle:ParticipationEvent')->findBy($id_user);
        return $this->render('UserBundle:Default:UserEvents.html.twig', array(
            'participationEvent' => $participationEvents,
        ));*/
    }

    /**
     * Creates a new participationEvent entity.
     *
     */
    public function newAction(/*Request $request*/ event $event  )
    {
        $em = $this->getDoctrine()->getManager();
        $users = $this->getUser();
        $check = $em->getRepository('GestEventBundle:ParticipationEvent')->checkPart($event->getId() , $users->getId());

        if(count($check) > 0)
        {
            return $this->render('participationevent/show2Error.html.twig');

        }
else{
        $participationEvent = new ParticipationEvent();
        /* $participationEvent = new Participationevent();
         $form = $this->createForm('GestEventBundle\Form\ParticipationEventType', $participationEvent);
         $form->handleRequest($request);
         $id=$request->query->get('id');
         echo $id;*/

        $participationEvent->setHeure(new \DateTime('now'));
        $participationEvent->setDate(new \DateTime('now'));
        $participationEvent->setEvent_id($event);
        $participationEvent->setUser_id($users);
        $event->setNbr_part($event->getNbrpart()+1);
        //if ($form->isSubmitted() && $form->isValid()) {
        // $participationEvent=$participationEvent->setEvent_id($id);

            $em->persist($participationEvent );
            $em->persist($event );
            $em->flush();
            return $this->redirectToRoute('participationevent_show', array('id' => $participationEvent->getId()));

         //}

        /*return $this->render('participationevent/new.html.twig', array(
            'participationEvent' => $participationEvent,
            'form' => $form->createView(),'id'=>$id
        ));*/
}
    }
    /**
     * Finds and displays a participationEvent entity.
     *
     */
    public function showAction(ParticipationEvent $participationEvent)
    {
        $em = $this->getDoctrine()->getManager();
        //$ev_id=$participationEvent->getEventid();
        $events = $em->getRepository('GestEventBundle:event')->find($participationEvent);
        return $this->render('participationevent/show.html.twig', array(
            'participationEvent' => $participationEvent, 'events' => $events,
        ));
    }
}
