<?php

namespace EspritApiBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session;
class DefaultController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {
        return $this->render('EspritApiBundle:Default:index.html.twig');
    }
    /**
     * @Route("/testApiRead")
     */
    public function readAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $projections = $em->getRepository('MainBundle:Projection')->findAll();
        $serializer = new Serializer([new ObjectNormalizer()]);
        $formated = $serializer->normalize($projections);
        return new JsonResponse( $formated );
    }

    /**
     * @Route("/pushData/{number}/{id}", name="pushData")
     * @Method("GET")
     */
    public function pushAction(Request $request,$id,$number)
    {


        $Salle = $this->getDoctrine()->getRepository('MainBundle:Salle')->find($request->attributes->get('id'));
        $n = $Salle->getNbreChaiseSalle() - $request->attributes->get('number');;
        $Salle->setNbreChaiseSalle($n);
        $em = $this->getDoctrine()->getManager();
        $em->persist($Salle);
        $em->flush();
        return new JsonResponse(array('1') );

    }
}
