<?php

namespace EspritBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\HttpFoundation\JsonResponse;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('EspritBundle:Default:index.html.twig');
    }


    public function showAction()
    {
        header('Access-Control-Allow-Origin:*');

        $em = $this->getDoctrine()->getManager();
        $club = $em->getRepository('MainBundle:Competition')->findAll();

        $serializer = new Serializer([new ObjectNormalizer()]);

        // $formated = $serializer->normalize($club);
        $normalizer = new ObjectNormalizer();
        $normalizer->setCircularReferenceLimit(1);
        // Add Circular reference handler
        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();

        });
        $normalizers = array($normalizer);
        $serializer = new Serializer($normalizers, $club);
        $formated = $serializer->normalize($club);

        return new JsonResponse($formated);
    }

    public function detailAction($id)
    {

        $em = $this->getDoctrine()->getManager();
        $club = $em->getRepository('MainBundle:Competition')->findBy(array('id'=>$id));

        $serializer = new Serializer([new ObjectNormalizer()]);

        // $formated = $serializer->normalize($club);
        $normalizer = new ObjectNormalizer();
        $normalizer->setCircularReferenceLimit(1);
        // Add Circular reference handler
        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();

        });

        $normalizers = array($normalizer);
        $serializer = new Serializer($normalizers, $club);
        $formated = $serializer->normalize($club);
        return new JsonResponse($formated);
    }

    public function mediaAction()
    {
        $em = $this->getDoctrine()->getManager();
        $images = $em->getRepository('MainBundle:Media')->findAll();
        $serializer = new Serializer([new ObjectNormalizer()]);

        // $formated = $serializer->normalize($club);
        $normalizer = new ObjectNormalizer();
        $normalizer->setCircularReferenceLimit(1);
        // Add Circular reference handler
        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();

        });

        $normalizers = array($normalizer);
        $serializer = new Serializer($normalizers, $images);
        $formated = $serializer->normalize($images);
        return new JsonResponse($formated);
    }

}
