<?php

namespace GestEventBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use SBC\NotificationsBundle\Builder\NotificationBuilder;
use SBC\NotificationsBundle\Model\NotifiableInterface;

/**
 * event
 *
 * @ORM\Table(name="event")
 * @ORM\Entity(repositoryClass="GestEventBundle\Repository\eventRepository")
 */
class event implements NotifiableInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="titre", type="string", length=255)
     */
    private $titre;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="\GestEventBundle\Entity\categorieEvent")
     * @ORM\JoinColumn(name="categorie", referencedColumnName="id")
     */
    private $categorie;

    /**
     * @var float
     *
     * @ORM\Column(name="prix", type="float")
     */
    private $prix;

    /**
     * @var int
     *
     * @ORM\Column(name="limit_max", type="integer")
     */
    private $limiteE_max;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="heure", type="time")
     */
    private $heure;

    /**
     * @var \DateTime
     *
     * @Assert\DateTime()
     * @Assert\GreaterThanOrEqual("today")
     * @ORM\Column(name="date", type="date")
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="lieu", type="string", length=255)
     */
    private $lieu;

    /**
     * @var int
     *
     * @ORM\Column(name="nbr_part", type="integer")
     */
    private $nbr_part;

    /**
     * @ORM\ManyToOne(targetEntity="\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user_id;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }



    /**
     * Set titre
     *
     * @param string $titre
     *
     * @return event
     */
    public function setTitre($titre)
    {
        $this->titre = $titre;

        return $this;
    }

    /**
     * Get titre
     *
     * @return string
     */
    public function getTitre()
    {
        return $this->titre;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return event
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set prix
     *
     * @param float $prix
     *
     * @return event
     */
    public function setPrix($prix)
    {
        $this->prix = $prix;

        return $this;
    }

    /**
     * Get prix
     *
     * @return float
     */
    public function getPrix()
    {
        return $this->prix;
    }

    /**
     * Set heure
     *
     * @param \DateTime $heure
     *
     * @return event
     */
    public function setHeure($heure)
    {
        $this->heure = $heure;

        return $this;
    }

    /**
     * Get heure
     *
     * @return \DateTime
     */
    public function getHeure()
    {
        return $this->heure;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return event
     */
    public function setDate(\DateTime $date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set lieu
     *
     * @param string $lieu
     *
     * @return event
     */
    public function setLieu($lieu)
    {
        $this->lieu = $lieu;

        return $this;
    }

    /**
     * Get lieu
     *
     * @return string
     */
    public function getLieu()
    {
        return $this->lieu;
    }

    /**
     * Set categorie
     *
     * @param \GestEventBundle\Entity\categorieEvent $categorie
     *
     * @return categorieEvent
     */
    public function setCategorieevent(\GestEventBundle\Entity\categorieEvent $categorie = null)
    {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * Get categorie
     *
     * @return \GestEventBundle\Entity\categorieEvent
     */
    public function getCategorieevent()
    {
        return $this->categorie;
    }


    /**
     * Set nbr_part
     *
     * @param int $nbr_part
     *
     * @return event
     */
    public function setNbr_part($nbr_part)
    {
        $this->nbr_part = $nbr_part;

        return $this;
    }

    /**
     * Get nbr_part
     *
     * @return int
     */
    public function getNbrpart()
    {
        return $this->nbr_part;
    }

    /**
     * Set int
     *
     * @param integer $user_id
     *
     * @return ParticipationEvent
     */
    public function setUser_id($user_id)
    {
        $this->user_id = $user_id;

        return $this;
    }

    /**
     * Get user_id
     *
     * @return integer
     */
    public function getUserid()
    {
        return $this->user_id;
    }

    /**
     * @return int
     */
    public function getLimiteEMax()
    {
        return $this->limiteE_max;
    }

    /**
     * @param int $limiteE_max
     */
    public function setLimiteEMax(int $limiteE_max)
    {
        $this->limiteE_max = $limiteE_max;
    }







    public function notificationsOnCreate(NotificationBuilder $builder)
    {
        $notification = new Notification();
        $notification->setTitle('Nouveau évènement')
                     ->setDescription($this->titre.' le '.$this->lieu)
                     ->setRoute('user_homepage')
                     ->setParameters(array('id'=>$this->id));
        $builder->addNotification($notification);
        return $builder;
    }

    public function notificationsOnUpdate(NotificationBuilder $builder)
    {
        $notification = new Notification();
        $notification->setTitle('MàJ évènement')
            ->setDescription($this->titre)
            ->setRoute('user_homepage')
            ->setParameters(array('id'=>$this->id));
        $builder->addNotification($notification);
        return $builder;
    }

    public function notificationsOnDelete(NotificationBuilder $builder)
    {
        $notification = new Notification();
        $notification->setTitle('Evènement supprimer')
            ->setDescription($this->titre)
            ->setRoute('user_homepage')
            ->setParameters(array('id'=>$this->id));
        $builder->addNotification($notification);
        return $builder;
    }


}

