<?php

namespace GestEventBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ParticipationEvent
 *
 * @ORM\Table(name="participation_event")
 * @ORM\Entity(repositoryClass="GestEventBundle\Repository\ParticipationEventRepository")
 */
class ParticipationEvent
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="heure", type="time")
     */
    private $heure;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date")
     */
    private $date;


    /**
     * @ORM\ManyToOne(targetEntity="\UserBundle\Entity\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user_id;

    /**
     * @ORM\ManyToOne(targetEntity="event")
     * @ORM\JoinColumn(name="event_id", referencedColumnName="id")
     */
    private $event_id;



    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set heure
     *
     * @param \DateTime $heure
     *
     * @return ParticipationEvent
     */
    public function setHeure($heure)
    {
        $this->heure = $heure;

        return $this;
    }

    /**
     * Get heure
     *
     * @return \DateTime
     */
    public function getHeure()
    {
        return $this->heure;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return ParticipationEvent
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set int
     *
     * @param integer $user_id
     *
     * @return ParticipationEvent
     */
    public function setUser_id($user_id)
    {
        $this->user_id = $user_id;

        return $this;
    }

    /**
     * Get user_id
     *
     * @return integer
     */
    public function getUserid()
    {
        return $this->user_id;
    }


    /**
     * Set int
     *
     * @param integer $event_id
     *
     * @return ParticipationEvent
     */
    public function setEvent_id($event_id)
    {
        $this->event_id = $event_id;

        return $this;
    }

    /**
     * Get event_id
     *
     * @return integer
     */
    public function getEventid()
    {
        return $this->event_id;
    }



}

