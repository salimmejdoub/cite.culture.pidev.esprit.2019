<?php

namespace GestEventBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class advancedEventControllerTest extends WebTestCase
{
    public function testAnnuler()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/annuler');
    }

    public function testParticipation()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/participation');
    }

    public function testRechercheavancee()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/rechercheavancee');
    }

}
