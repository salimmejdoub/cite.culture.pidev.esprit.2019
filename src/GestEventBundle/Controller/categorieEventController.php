<?php

namespace GestEventBundle\Controller;

use GestEventBundle\Entity\categorieEvent;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Categorieevent controller.
 *
 */
class categorieEventController extends Controller
{
    /**
     * Lists all categorieEvent entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $categorieEvents = $em->getRepository('GestEventBundle:categorieEvent')->findAll();

        return $this->render('categorieevent/index.html.twig', array(
            'categorieEvents' => $categorieEvents,
        ));
    }

    /**
     * Creates a new categorieEvent entity.
     *
     */
    public function newAction(Request $request)
    {
        $user=$this->getUser();
        $categorieEvent = new Categorieevent();
        $form = $this->createForm('GestEventBundle\Form\categorieEventType', $categorieEvent);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $categorieEvent->setUserid($user);
            $em->persist($categorieEvent);
            $em->flush();

            return $this->redirectToRoute('categorieevent_show', array('id' => $categorieEvent->getId()));
        }

        return $this->render('categorieevent/new.html.twig', array(
            'categorieEvent' => $categorieEvent,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a categorieEvent entity.
     *
     */
    public function showAction(categorieEvent $categorieEvent)
    {
        $deleteForm = $this->createDeleteForm($categorieEvent);

        return $this->render('categorieevent/show.html.twig', array(
            'categorieEvent' => $categorieEvent,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing categorieEvent entity.
     *
     */
    public function editAction(Request $request, categorieEvent $categorieEvent)
    {
        $deleteForm = $this->createDeleteForm($categorieEvent);
        $editForm = $this->createForm('GestEventBundle\Form\categorieEventType', $categorieEvent);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('categorieevent_index');
        }

        return $this->render('categorieevent/edit.html.twig', array(
            'categorieEvent' => $categorieEvent,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a categorieEvent entity.
     *
     */
    public function deleteAction(/*Request $request, categorieEvent $categorieEvent*/$id)
    {
        if ($id > 0) {
            $em = $this->getDoctrine()->getManager();
            $categorie = $em->getRepository('GestEventBundle:categorieEvent')->find($id);
            if (!$categorie instanceof categorieEvent)
                throw $this->createNotFoundException('La page n\'existe pas.');

            $em->remove($categorie);
            $em->flush($categorie);
            return $this->redirectToRoute('categorieevent_index');

        } else {
            throw $this->createNotFoundException('La page n\'existe pas.');
        }
        /*
        $form = $this->createDeleteForm($categorieEvent);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($categorieEvent);
            $em->flush();
        }

        return $this->redirectToRoute('categorieevent_index');*/
    }

    /**
     * Creates a form to delete a categorieEvent entity.
     *
     * @param categorieEvent $categorieEvent The categorieEvent entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(categorieEvent $categorieEvent)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('categorieevent_delete', array('id' => $categorieEvent->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
