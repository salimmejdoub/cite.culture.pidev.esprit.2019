<?php

namespace GestEventBundle\Controller;

use GestEventBundle\Entity\event;
use GestEventBundle\Entity\ParticipationEvent;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\Date;
use UserBundle\Entity\User;

class advancedEventController extends Controller
{


    public function confirmerAction(ParticipationEvent $participationEvent)
    {


        return $this->render('GestEventBundle:advancedEvent:confirmer.html.twig'
        , array(
            'ParticipationEvent'=>$participationEvent
        ));
    }

    public function annulerAction(ParticipationEvent $participationEvent)
    {
        $id=$participationEvent->getId();
        $idEvent=$participationEvent->getEventid()->getId();


        if ($id > 0) {


            $em = $this->getDoctrine()->getManager();
            $part = $em->getRepository('GestEventBundle:ParticipationEvent')->find($id);
            $event = $em->getRepository('GestEventBundle:event')->find($idEvent);
            if (!$part instanceof ParticipationEvent)
                throw $this->createNotFoundException('La page n\'existe pas.');
            $em->remove($part);
            $em->flush($part);
            $event->setNbr_part($participationEvent->getEventid()->getNbrpart()-1);
            $em->flush($event);

            return $this->redirectToRoute('user_homepage');

        } else {
            throw $this->createNotFoundException('La page n\'existe pas.');
        }

    }


public function topthreeAction()
    {
        $em = $this->getDoctrine()->getManager();
        $topthree = $em->getRepository('GestEventBundle:ParticipationEvent')->topthree();

        return $this->render('GestEventBundle:advancedEvent:topthree.html.twig', array(
            'topthree'=> $topthree,
        ));
    }

    public function gratuitAction()
    {
        $em = $this->getDoctrine()->getManager();
        $gratuit = $em->getRepository('GestEventBundle:ParticipationEvent')->gratuit();

        return $this->render('GestEventBundle:advancedEvent:gratuit.html.twig', array(
            'gratuit'=> $gratuit,
        ));
    }


    public function myEventAction(User $user)
    {
        $em = $this->getDoctrine()->getManager();
        $participationEvents = $em->getRepository('GestEventBundle:ParticipationEvent')->findUserEvents($user->getId());
        return $this->render('UserBundle:Default:UserEvents.html.twig', array(
            'participationEvents' => $participationEvents,
        ));
    }

    public function topclientAction()
    {
        $em = $this->getDoctrine()->getManager();
        $topclient = $em->getRepository('GestEventBundle:ParticipationEvent')->topclient();

        return $this->render('/participationevent/topclient.html.twig', array(
            'topclient'=> $topclient,
        ));
    }

}
