<?php

namespace GestEventBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class NotificationController extends Controller
{
    public function showAction(){
        $notification=$this->getDoctrine()->getManager()->getRepository('GestEventBundle:Notification')->findAll();
        return $this->render('UserBundle:Default:index.html.twig' , array('notification'=>$notification));
    }
}
