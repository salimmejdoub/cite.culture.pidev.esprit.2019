<?php

namespace GestEventBundle\Controller;

use GestEventBundle\Entity\event;
use GestEventBundle\Entity\ParticipationEvent;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use UserBundle\Entity\User;

/**
 * Participationevent controller.
 *
 */
class ParticipationEventController extends Controller
{
    /**
     * Lists all participationEvent entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $participationEvents = $em->getRepository('GestEventBundle:ParticipationEvent')->findAll();

        return $this->render('participationevent/index.html.twig', array(
            'participationEvents' => $participationEvents,
        ));
    }
    /**
     * Lists all participationEvent entities.
     *
     */
    public function index2Action()
    {
        $em = $this->getDoctrine()->getManager();

        $events = $em->getRepository('GestEventBundle:event')->findAll();

        return $this->render('participationevent/allEvent.html.twig', array(
            'events' => $events
        ));
    }




    /**
     * Displays a form to edit an existing participationEvent entity.
     *
     */
    public function editAction(Request $request, ParticipationEvent $participationEvent)
    {
        $deleteForm = $this->createDeleteForm($participationEvent);
        $editForm = $this->createForm('GestEventBundle\Form\ParticipationEventType', $participationEvent);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('participationevent_edit', array('id' => $participationEvent->getId()));
        }

        return $this->render('participationevent/edit.html.twig', array(
            'participationEvent' => $participationEvent,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a participationEvent entity.
     *
     */
    public function deleteAction($id)
    {
        if ($id > 0) {
            $em = $this->getDoctrine()->getManager();
            $part = $em->getRepository('GestEventBundle:ParticipationEvent')->find($id);
            if (!$part instanceof ParticipationEvent)
                throw $this->createNotFoundException('La page n\'existe pas.');

            $em->remove($part);
            $em->flush($part);
            return $this->redirectToRoute('participationevent_index');

        } else {
            throw $this->createNotFoundException('La page n\'existe pas.');
        }

    }

    /**
     * Creates a form to delete a participationEvent entity.
     *
     * @param ParticipationEvent $participationEvent The participationEvent entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(ParticipationEvent $participationEvent)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('participationevent_delete', array('id' => $participationEvent->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }



}
