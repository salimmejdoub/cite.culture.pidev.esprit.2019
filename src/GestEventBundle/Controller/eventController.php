<?php

namespace GestEventBundle\Controller;

use GestEventBundle\Entity\event;
use GestEventBundle\Entity\Notification;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * Event controller.
 *
 */
class eventController extends Controller
{
    /**
     * Lists all event entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $events = $em->getRepository('GestEventBundle:event')->findAll();

        return $this->render('event/index.html.twig', array(
            'events' => $events
        ));
    }


    /**
     * Creates a new event entity.
     *
     */
    public function newAction(Request $request)
    {   $userID=$this->getUser();
        $event = new event();
        $form = $this->createForm('GestEventBundle\Form\eventType', $event);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $event->setNbr_part(0);
            $event->setUser_id($userID);
            $event->setLieu("Cite de la culture");
            $em->persist($event);
            $em->flush();
          /*  $notification=new Notification();
            $notification
                ->setTitle('Nouveau évènement!')
                ->setDescription($event->getTitre())
                ->setRoute('user_homepage')
                ->setParameters(array('id' => $event->getId()))
                ;
            $em->persist($event);
            $em->flush();
            $pusher=$this->get('mrad.pusher.notificaitons');
            $pusher->trigger($notification);*/
            return $this->redirectToRoute('event_show', array('id' => $event->getId()));
        }

        return $this->render('event/new.html.twig', array(
            'event' => $event,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a event entity.
     *
     */
    public function showAction(event $event)
    {
        $deleteForm = $this->createDeleteForm($event);

        return $this->render('event/show.html.twig', array(
            'event' => $event,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing event entity.
     *
     */
    public function editAction(Request $request, event $event)
    {
        $deleteForm = $this->createDeleteForm($event);
        $editForm = $this->createForm('GestEventBundle\Form\eventType', $event);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

           /* $notification=new Notification();
            $notification
                ->setTitle('Mise à jour évènement!')
                ->setDescription($event->getTitre())
                ->setRoute('user_homepage')
                ->setParameters(array('id' => $event->getId()))
            ;
            $this->getDoctrine()->getManager()->persist($event);
            $this->getDoctrine()->getManager()->flush();
            $pusher=$this->get('mrad.pusher.notificaitons');
            $pusher->trigger($notification);*/

            return $this->redirectToRoute('event_index', array('id' => $event->getId()));
        }

        return $this->render('event/edit.html.twig', array(
            'event' => $event,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a event entity.
     *
     */
    public function deleteAction($id)
    {
        if ($id > 0) {
            $em = $this->getDoctrine()->getManager();
            $event = $em->getRepository('GestEventBundle:event')->find($id);
            if (!$event instanceof event)
                throw $this->createNotFoundException('La page n\'existe pas.');

            $em->remove($event);
            $em->flush($event);
            return $this->redirectToRoute('event_index');

        } else {
            throw $this->createNotFoundException('La page n\'existe pas.');
        }
    }

    /**
     * Creates a form to delete a event entity.
     *
     * @param event $event The event entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(event $event)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('event_delete', array('id' => $event->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
